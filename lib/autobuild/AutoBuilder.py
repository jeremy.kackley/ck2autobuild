from Building_Manager import Building_Manager
#pylint: disable=E0401
from ck2.code.base import ScriptedEffects, ScriptedTriggers
from ck2.modbase import ModBase, gen_readme_report
from common.effects import SCRIPTED_EFFECTS
from common.triggers import SCRIPTED_TRIGGERS
from ConfigMenuGenerator import ConfigMenuGenerator
from defines import GRAPH_ABOUT_STR
from Events.Manager import Manager as EM
from ModifierGenerator import ModifierGenerator
from DiscountCalculator import DiscountCalculator
import defines


class AutoBuilder2(ModBase):
    '''
        New and improved AutoBuilder
    '''

    def __init__(self, buildings=None):
        ModBase.__init__(self)
        # used by ModBase:
        self.mod_folder_mask = 'AutoBuild_{}'
        self.mod_file_mask = 'AutoBuild_{}.mod'
        self.vanilla_mod_name = 'AutoBuild'
        self.submod_mod_name = '{} (AB SubMod)'
        self.filename_mask = 'AutoBuild_{}_{}'
        self.gs_cache_name = 'AutoBuild_TranslationCache.xml'
        self.st = ScriptedTriggers()
        self.se = ScriptedEffects()
        self.modifiers = ModifierGenerator(self)
        self.config = ConfigMenuGenerator(self)
        self.dc = DiscountCalculator()
        self.bm = Building_Manager(self)
        self.em = EM(self)
        if buildings:
            for bldg in buildings:
                self.add(bldg)

    def add_building(self, building):
        self.bm.add(building)

    def add_tech(self, tech):
        self.dc.add_technology(tech)

    def add_job_action(self, job_action):
        self.dc.add_job_action(job_action)

    def add_artifact(self, artifact):
        self.dc.add_artifact(artifact)

    def add_bloodline(self, bloodline):
        self.dc.add_bloodline(bloodline)

    def add_event_modifier(self, modifier):
        self.dc.add_event_modifier(modifier)

    def has_unique_buildings(self):
        return self.bm.has_unique_buildings()

    @gen_readme_report('placeholder buildings')
    def _gen_faux(self):
        count = 0
        for building in self.bm.gen_phs():
            fn = 'abph_buildings_{}.txt'.format(building.type)
            self.buildings.add(building, fn)
            count += 1
        for camp in self.bm.gen_camps():
            self.buildings.add(camp, 'workcamps.txt')
            count += 1
        return count

    @gen_readme_report('modifiers')
    def _gen_modifiers(self):
        count = 0
        for building in self.bm.get_buildings():
            count += 1
            self.event_modifiers.add(
                self.modifiers.create(building), 'modifiers.txt')
        return count

    @gen_readme_report('config menu decisions')
    def _gen_config(self):
        count = 0
        for dec in self.config.create_all():
            count += 1
            self.decisions.add(dec, 'menu.txt')
        return count

    @gen_readme_report('localizations')
    def _gen_loca(self):
        count = 0
        for thing in (
                self.bm,
                self.modifiers,
                self.config, ):
            for loc in thing.gen_loca():
                count += 1
                self.localizations.add(loc)
        return count

    @gen_readme_report('graphs')
    def _gen_graphs(self):
        if not hasattr(self, 'graph'):
            self.add_folder('graphs', 'graphs\\')
        for atype, graph in self.bm.sets.get_tree_graphs().items():
            if len(graph)>0:
                if defines.GENERATE_GRAPHS_GML:
                    name = '{}.gml'.format(atype)
                    data = graph.to_gml()
                    self.graphs.add_preserialized(data, name)
                if defines.GENERATE_GRAPHS_TGF:
                    name = '{}.tgf'.format(atype)
                    data = graph.to_tgf()
                    self.graphs.add_preserialized(data, name)
                if defines.GENERATE_GRAPHS_DOT:
                    name = '{}.dot'.format(atype)
                    data = graph.to_dot()
                    self.graphs.add_preserialized(data, name)
        self.graphs.add_preserialized(GRAPH_ABOUT_STR, 'about.txt')

    @gen_readme_report('scripted effects')
    def _gen_se(self):
        count = 0
        for name,text in self.dc.gen_scripted_effects():
            self.se.add(name,text)
        for thing in SCRIPTED_EFFECTS:
            for object in thing.get_object():
                self.se.add_object(object)
        for thing in self.se.get_items_by_category():
            fn = 'abse_{}.txt'.format(thing.category)
            self.scripted_effects.add(thing, fn)
            count += 1
        return count

    @gen_readme_report('scripted triggers')
    def _gen_st(self):
        count = 0
        self.bm.gen_scripted_triggers()
        for thing in SCRIPTED_TRIGGERS:
            for object in thing.get_object():
                self.st.add_object(object)
        for thing in self.st.get_items_by_category():
            fn = 'abst_{}.txt'.format(thing.category)
            self.scripted_triggers.add(thing, fn)
            count += 1
        return count

    def generate(self):
        self.clear()
        self._gen_faux()
        self._gen_modifiers()
        # must be called first, because start events call them.
        self._gen_config()
        self._gen_loca()
        self._gen_graphs()
        self._gen_se()
        self._gen_st()
        if defines.DEBUG_SETS_REPORT:
            self.add_readme('sets_report.txt', self.bm.sets.report())
