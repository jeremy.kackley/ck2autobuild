from .. import DiscountCalculator
from ck2.constructs import JobAction, Technology, EventModifier, BLOCK
from ck2.utilities.parse_functions import parse
import unittest

#maybe move these to a data directory, ultimately.
JOB_ACTION_TEST = """
action_oversee_construction = {
	attribute = stewardship
	
	potential = {
		FROM = {
			job_treasurer = {
				NOT = {
					has_character_modifier = in_seclusion
				}
			}
		}
	}

	trigger = {
		any_province_lord = {
			character = FROM
		}
	}
	
	local_build_time_modifier = -0.025
	
	events = { 923 924 }
}"""
TECH_TEST = """
	TECH_CONSTRUCTION = { #improved construction of all buildings
		modifier = {
			LOCAL_BUILD_TIME_MODIFIER = -0.25
			LOCAL_BUILD_COST_MODIFIER = -0.25
		}
		1= {
		
		}
		2 = {
			add_building = hospital_building_1
			add_building = leper_colony_1
			add_building = soup_kitchen_1
		}
		3 = {
			add_building = hospital_building_2
			add_building = chapel_1
			add_building = translation_house_1
			add_building = pilgrims_inn_1
			add_building = library_1
			
		}
		4 = {
			add_building = hospital_building_3
			add_building = pharmacology_laboratory_1
			add_building = medical_academy_1
			add_building = observatory_1
		}
		5 = {
			add_building = hospital_building_4
		}
		6 = {
			add_building = hospital_building_5
		}
	}
"""
BAD_TECH="""
	TECH_LIGHT_INFANTRY = {
		modifier = {
			ARCHERS_OFFENSIVE = 0.6
			LIGHT_INFANTRY_OFFENSIVE = 0.6
			LIGHT_INFANTRY_DEFENSIVE = 0.6
			ARCHERS_DEFENSIVE = 0.6
		}	
	}
"""
MODIFIER_TEST = """
master_builder = {
	local_build_time_modifier = -0.05
	icon = 12
}
"""
MODIFIER_INC_TEST = """
sabotaged_building = {
	local_build_time_modifier = 0.3
	icon = 30
}"""

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

class TestDiscountCalculator(unittest.TestCase):
	def test_add_job_action(self):
		x = DiscountCalculator.DiscountCalculator()
		x.add_job_action(JobAction(BLOCK("test")))
		#assert that it did not insert junk JobAction
		assert (len(x.job_actions()) == 0)
		x.add_job_action(JobAction(parse(JOB_ACTION_TEST)[0]))
		#assert that the number of action effect is < max attribute
		assert (len(x.job_actions()) < x.MAX_ATTRIBUTE)

	def test_mods(self):
		x = DiscountCalculator.DiscountCalculator()
		#assert that it did not insert junk modifiers
		x.add_event_modifier(EventModifier(BLOCK("test")))
		assert (len(x.event_modifiers()) == 0)
		#assert that it did insert a good modifier
		x.add_event_modifier(EventModifier(parse(MODIFIER_TEST)[0]))
		assert (len(x.event_modifiers()) == 1)
		#assert that the proper time and cost effect from
		#the test were applied
		assert(x.event_modifiers()[0].time==-0.05)
		assert(x.event_modifiers()[0].cost==0)

	def test_job_action_breakpoints(self):
		x = DiscountCalculator.DiscountCalculator()
		x.add_job_action(JobAction(parse(JOB_ACTION_TEST)[0]))
		assert (len(x.time_breakpoints(100)) < x.MAX_STEPS * 2)
		#assert that cost breakpoints with only an action that doesn't affect
		#cost doesn't yield any breakpoints.
		cbp = x.cost_breakpoints(100)
		assert (len(cbp) == 1)
		assert (cbp[0].flag == x.default_cost_flag)
		assert (cbp[0].value == 100)

	def test_flags(self):
		#assert that it honors MAX_STEPS
		x = DiscountCalculator.DiscountCalculator()
		assert (len(x.possible_cost_flags()) <= x.MAX_STEPS * 2 + 1)
		assert (len(x.possible_time_flags()) <= x.MAX_STEPS * 2 + 1)

	def test_possible_breakpoints(self):
		x = DiscountCalculator.DiscountCalculator()
		#assert that with mo modiifers added, there's only the default
		# breakpoints
		assert (len(x.possible_cost_breakpoints()) == 1)
		assert (len(x.possible_time_breakpoints()) == 1)
		x.add_job_action(JobAction(parse(JOB_ACTION_TEST)[0]))
		#assert that after adding a job action that only modifies tech, time has
		# the proper range of breakpoints, but cost still only has the one.    
		assert (len(x.possible_cost_breakpoints()) == 1)
		assert (len(x.possible_time_breakpoints()) == x.MAX_STEPS + 1)

	def test_tech(self):
		x = DiscountCalculator.DiscountCalculator()
		#assert that it did not insert tech that doesn't provide any benefits.
		x.add_technology(Technology('TECH_GROUP_MILITARY', parse(BAD_TECH)[0]))
		assert (len(x.technology()) == 0)
		#assert that it did insert a good tech
		x.add_technology(Technology('TECH_GROUP_ECONOMY', parse(TECH_TEST)[0]))
			#each good tech should = 8 effects, e.g. tech_level 1 through tech_level_8
		assert (len(x.technology()) == 8) 
	
	def test_increment(self):
		#verify that modifiers that increase build time or cost
		#are handled properly.
		x = DiscountCalculator.DiscountCalculator()
		x.add_event_modifier(EventModifier(parse(MODIFIER_INC_TEST)[0]))
		#assert that this increased max_build_time
		assert(x.max_build_time_increase>0)
		#assert that possible breakpoints now includes
		#positive breakpoints
		assert (len(x.possible_time_breakpoints()) == x.MAX_STEPS + 1)
		x.add_event_modifier(EventModifier(parse(MODIFIER_TEST)[0]))
		#assert that possible breakpoints now includes
		#positive breakpoints and negative ones
		assert (len(x.possible_time_breakpoints()) == x.MAX_STEPS*2 + 1)