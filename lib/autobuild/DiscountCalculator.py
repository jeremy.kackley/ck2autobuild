"""Summary
# TODO something isn't working here; add logging to every decision and verify which ones are not getting checked.
#   my dumb test indicates only ROOT character modifiers are getting checked...
"""
import itertools, collections
from operator import itemgetter

from jinja2 import Environment, FileSystemLoader, Template
from os.path import abspath, dirname, join
import defines

env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')),
    lstrip_blocks=True,
    trim_blocks=True)

BreakPoint = collections.namedtuple(
    'BreakPoint', ['flag', 'value'], verbose=False)
JobActionEffect = collections.namedtuple(
    'JobActionEffect', ['name', 'attribute_name', 'effects'], verbose=False)
TechEffect = collections.namedtuple(
    'TechEffect', ['name', 'level', 'level_not', 'time', 'cost'],
    verbose=False)
ModEffect = collections.namedtuple(
    'ModEffect', ['name', 'time', 'cost'], verbose=False)
BloodlineEffect = collections.namedtuple(
    'BloodlineEffect', ['name', 'time', 'cost'], verbose=False)
ArtifactEffect = collections.namedtuple(
    'ArtifactEffect', ['name', 'equipable', 'has_conditions', 'time', 'cost'], verbose=False)


def cast(value):
    if value:
        return float(value)
    return 0.0


def consume(iterator, n):
    '''Advance the iterator n-steps ahead. If n is none, consume entirely.'''
    collections.deque(itertools.islice(iterator, n), maxlen=0)


class DiscountCalculator:
    """
    This class computes discount breakpoints based on the available discounts.
    
    For example, in Vanilla TECH_CONSTRUCTION yields a build cost / build time
        modifier of up to -25% (e.g. -0.25)
    
    The job action ot assist construction can, for characters with high stewardship,
        yield a modifier over 100%, effectively reducing the build time to 1.
    
    This class takes into account job actions / technology and other modifiers
        (e.g. event_modifiers) and both creates checks for them as well as 
        code to compute the effective discount. 

    DevOptions:
        What would generate the smallest block of code?
            a nested if might be rather long if I parse say, 100 modifier sources
            that have build_time modifiers.  
        Though it pains me, a variable might be more appropriate; e.g. set / check variables.
            for calculation only; I can set flags at the end...
        On the other hand, if I set a set of province flags, my trigger switch is much smaller
            ab_prov_build_mods_set
            ab_prov_bt_mod_neg_x
            ab_prov_bc_mod_neg_x
            ab_prov_bt_mod_pos_x
            ab_prov_bc_mod_pos_x
        Note that positive modifiers actually increase the build cost or time.
    
    Lets re-evaluate what things do:
        modifiers, technology, and job actions are for building the 'evaluate' block.abs
        this renders the named tuples unecessary, since the template can just access the base 
        class
    Breakpoints is for calculating the pay, refund, or build blocks and concerns itself with,
        given a cost/time, caluclating how the province modifiers will affect that cost/time.

    Useful events for testing things:
        event MNM.78008 = add's smugglers ring to a province of yours.
        event MNM.70006 = add's 'inspired peasentry to a province of yours'
        event 923 = add's master builder to character
        get_all_artifacts = add's all artifacts (some of which passively improve build times, /shrug)
        event HF.24047 = adds 'the builder' bloodline

    """

    def __init__(self):
        """Summary
        """
        #first, lets set up some constants that control how discounts are calculated.
        self.MAX_ATTRIBUTE = 100
        self.MIN_TIME_CHANGE = 0  #its not worth calculating a step if the change is < this
        self.MIN_COST_CHANGE = 0  #its not worth calculating a step if the change is < this
        self.MAX_STEPS = 10  #max number of steps for positive or negative increases
        self.MIN_COST = 1
        self.MIN_TIME = 1
        self._artifacts = []
        self._bloodlines = []
        self._modifiers = []
        self._global_mofidiers = []
        self._technology = []
        self._job_actions = []
        self.clear_max_discounts()
        self._time_flag_mask = 'ab_prov_bt_mod_{}_{:02d}'
        self._cost_flag_mask = 'ab_prov_bc_mod_{}_{:02d}'
        self.evaluated_time_flag = 'ab_evaluated_time_flag'
        self.evaluated_cost_flag = 'ab_evaluated_cost_flag'
        self.time_variable = 'ab_prov_build_mods_time'
        self.cost_variable = 'ab_prov_build_mods_cost'
        self.default_time_flag = 'ab_prov_bt_mod_def'
        self.default_cost_flag = 'ab_prov_bc_mod_def'       
        #don't change or override these min/max flags without changing the rest of the class. 
        self.max_time_flag=self._time_flag_mask.format('inc', 10)
        self.min_time_flag=self._time_flag_mask.format('dec', 10)
        self.max_cost_flag=self._cost_flag_mask.format('inc', 10)
        self.min_cost_flag=self._cost_flag_mask.format('dec', 10)
        self.debug=defines.DEBUG_DISCOUNTS # controls generation of logging messages to verify conditions are checked.
        self.enable_bloodlines = False 
        #to implement bloodlines I would have to override them and add default flags to the builder ones...
        #   and even that might not work.  Gonna wait till they add a 'bloodline_type' command.        

    def possible_time_flags(self):
        flags = []        
        for asdf in ('inc', 'dec'):
            for x in range(1, self.MAX_STEPS + 1):
                flags.append(self._time_flag_mask.format(asdf, x))
            if self.default_time_flag not in flags:
                flags.append(self.default_time_flag)
        return flags

    def possible_cost_flags(self):
        flags = []
        for asdf in ('inc', 'dec'):
            for x in range(1, self.MAX_STEPS + 1):
                flags.append(self._cost_flag_mask.format(asdf, x))
            if self.default_cost_flag not in flags:
                flags.append(self.default_cost_flag)
        return flags

    def clear_max_discounts(self):
        self._calculated_discounts = False
        self.max_build_time_redction = 0.0
        self.max_build_time_increase = 0.0
        self.max_build_cost_reduction = 0.0
        self.max_build_cost_increase = 0.0

    def add_artifact(self, artifact): 
        if artifact.build_time_modifier() or artifact.build_cost_modifier():
            time_eff = cast(artifact.build_time_modifier())
            cost_eff = cast(artifact.build_cost_modifier())
            equipable=True
            if not artifact.slot():
                equipable=False
            active=True
            if not artifact.active():
                active=False
            self._artifacts.append(ArtifactEffect(artifact.name(), equipable, active, time_eff, cost_eff))
            self._add_time_cost(time_eff, cost_eff)

    def add_bloodline(self, bloodline): 
        if bloodline.build_time_modifier() or bloodline.build_cost_modifier():
            time_eff = cast(bloodline.build_time_modifier())
            cost_eff = cast(bloodline.build_cost_modifier())
            self._bloodlines.append(BloodlineEffect(bloodline.name(), time_eff, cost_eff))
            self._add_time_cost(time_eff, cost_eff)

    def add_event_modifier(self, mod): 
        #local modifiers are handled differently than global modifiers
        if mod.local_build_time_modifier() or mod.local_build_cost_modifier():
            time_eff = cast(mod.local_build_time_modifier())
            cost_eff = cast(mod.local_build_cost_modifier())
            self._modifiers.append(ModEffect(mod.name(), time_eff, cost_eff))
            self._add_time_cost(time_eff, cost_eff)
        if mod.build_time_modifier() or mod.build_cost_modifier():
            time_eff = cast(mod.build_time_modifier())
            cost_eff = cast(mod.build_cost_modifier())
            self._global_mofidiers.append(ModEffect(mod.name(), time_eff, cost_eff))
            self._add_time_cost(time_eff, cost_eff)

    def add_technology(self, tech):
        if tech.local_build_time_modifier() or tech.local_build_cost_modifier(
        ):
            basetime = cast(tech.local_build_time_modifier())
            basecost = cast(tech.local_build_cost_modifier())
            for level in range(1, 9):
                level_fraction = float(level) / 8.0
                time = basetime * level_fraction
                cost = basecost * level_fraction
                if level < 8:
                    level_not = level + 1
                else:
                    level_not = None
                self._technology.append(
                    TechEffect(tech.name(), level, level_not, time, cost))
            self._add_time_cost(basetime, basecost)

    def add_job_action(self, action):
        if action.local_build_time_modifier(
        ) or action.local_build_cost_modifier():
            basetime = cast(action.local_build_time_modifier())
            basecost = cast(action.local_build_cost_modifier())
            attributes = range(1, self.MAX_ATTRIBUTE).__iter__()
            count = 0
            effects = []
            for attr in attributes:
                #this is intended to stagger the later samples; it's
                #simply not worth the granularity; also attributes over about 25
                #probably mean you are cheating or modding.
                if attr > 20:
                    consume(attributes, count)
                    count += 1
                time = basetime * float(attr)
                cost = basecost * float(attr)
                effects.append((attr, time, cost))
            effects.reverse()
            self._job_actions.append(
                JobActionEffect(action.name(), action.attribute(), effects))
            if time < -0.99999:
                time = -0.99999
            if cost < -0.99999:
                cost = -0.99999
            self._add_time_cost(time, cost)

#How I arrived at the somewhat abritrary value of -0.99999
#In the following table, days can be thought of build time, and 
#years is simply a rough estimate of the number of years (days/365) 
#implied by it.  Max Discount is the 'build time' given 'capped out'
#discounts.
#While it does break for extreme values; frankly any building that takes more 
#   than a few decades seems pretty unlikely, so safely handling
#   values less thana  few centuries seems a safe margin.
#
#Similarly, building costs much in excess of 10k gold seem pretty unlikely; 
#   and you ar eunlikely to notice that a million gold building costs 10 gold 
#   instead of 1...
#
#Days: 10	 Years: 0	 MaxDiscount: 1
#Days: 100	 Years: 0	 MaxDiscount: 1
#Days: 1000	 Years: 2	 MaxDiscount: 1
#Days: 10000	 Years: 27	 MaxDiscount: 1
#Days: 100000	 Years: 273	 MaxDiscount: 1
#Days: 1000000	 Years: 2739	 MaxDiscount: 10
#Days: 10000000	 Years: 27397	 MaxDiscount: 100
#Days: 100000000	 Years: 273972	 MaxDiscount: 1000
#
#FWIW, table generated with this code:
#for x in [1,2,3,4,5,6,7,8]:
#	cost=10**x
#	discount=int(cost*-0.99999)
#	final=cost+discount
#	print 'Days: {}\t Years: {}\t MaxDiscount: {}'.format(cost,cost/365,final)

    def _add_time_cost(self, time, cost):
        if time < 0:
            self.max_build_time_redction += time
        if time > 0:
            self.max_build_time_increase += time
        if cost < 0:
            self.max_build_cost_reduction += cost
        if cost > 0:
            self.max_build_cost_increase += cost
        if self.max_build_cost_reduction < -0.99999:
            self.max_build_cost_reduction = -0.99999
        if self.max_build_time_redction < -0.99999:
            self.max_build_time_redction = -0.99999

    def gen_scripted_effects(self):
        ret = []
        names=[defines.SE_CLR_PROVINCE_FLAGS,defines.SE_FLAG_PROVINCE]
        templates=['clear_province_flags.j2','flag_province_modifiers.j2']
        for name, tname in zip(names,templates):
            template = env.get_template(tname)
            ret.append([name, template.render(dc=self)])
        return ret

    def _generic_steps(self, flags, max_increase, max_reduction):
        #starting cumlative at max allows the steps to
        #go from most expensive to least expensive.
        increases = []
        for x in range(1, self.MAX_STEPS + 1):
            increases.append(max_increase / float(self.MAX_STEPS) * x)
        reduces = [0.0]
        for x in range(1, self.MAX_STEPS + 1):
            reduces.append(max_reduction / float(self.MAX_STEPS) * x)
        ret = zip(flags, increases + reduces)
        ret.sort(key=itemgetter(1))
        ret.reverse()
        return ret

    def _time_steps(self):
        time_flags = self.possible_time_flags()
        return self._generic_steps(time_flags, self.max_build_time_increase,
                                   self.max_build_time_redction)

    def _cost_steps(self):
        cost_flags = self.possible_cost_flags()
        return self._generic_steps(cost_flags, self.max_build_cost_increase,
                                   self.max_build_cost_reduction)

    def possible_time_breakpoints(self):
        ret = []
        for time_flag, time in self._time_steps():
            #if time != 0.0 or time_flag == self.default_time_flag:
            ret.append(BreakPoint(time_flag, time))
        return ret

    def possible_cost_breakpoints(self):
        ret = []
        for flag, cost in self._cost_steps():
            #if cost != 0.0 or flag == self.default_cost_flag:
            ret.append(BreakPoint(flag, cost))
        return ret

    def time_breakpoints(self, build_time):
        """Given the build_time value, calculates the flags and their effect

        Note that flag might be None, in which case there are no modifiers
        worth applying in this case (e.g. the building is extremely quick already)

        Returns:
            List: A list of BreakPoint namedtuples with flag and value members.
        """
        ret = []
        for time_flag, time in self._time_steps():
            if time != 0 or time_flag == self.default_time_flag:
                time = build_time + int(build_time * time)
                if time < self.MIN_TIME:
                    time = self.MIN_TIME
                ret.append(BreakPoint(time_flag, time))
        return ret

    def cost_breakpoints(self, build_cost):
        """Given the build cost value, calculates the flags and their effect
        
        Note that flag might be None, in which case there are no modifiers
        worth applying in this case (e.g. the building is extremely cheap already)

        Returns:
            List: A list of BreakPoint namedtuples with flag and value members.
        """
        ret = []
        for cost_flag, cost in self._cost_steps():
            if cost != 0 or cost_flag == self.default_cost_flag:
                cost = build_cost + int(build_cost * cost)
                if cost < self.MIN_COST:
                    cost = self.MIN_COST
                ret.append(BreakPoint(cost_flag, cost))
        return ret

    def artifacts(self):
        """Returns a list of tuples indicating what modifiers globally affect
        build time or build cost and by how much.

        Used by templates to build blocks checking for the modifier.
        
        Returns:
            List: A list of ModEffect namedtutples with the modifier, time effect and cost effect
        """
        return self._artifacts

    def permanent_artifacts(self):
        """Returns a list of tuples indicating what modifiers globally affect
        build time or build cost and by how much.

        Used by templates to build blocks checking for the modifier.
        
        Returns:
            List: A list of ModEffect namedtutples with the modifier, time effect and cost effect
        """
        return (x for x in self.artifacts() if not x.equipable and not x.has_conditions)

    def conditional_artifacts(self):
        """Returns a list of tuples indicating what modifiers globally affect
        build time or build cost and by how much.

        Used by templates to build blocks checking for the modifier.
        
        Returns:
            List: A list of ModEffect namedtutples with the modifier, time effect and cost effect
        """
        return (x for x in self.artifacts() if x.equipable or x.has_conditions)

    def bloodlines(self):
        """Returns a list of tuples indicating what modifiers globally affect
        build time or build cost and by how much.

        Used by templates to build blocks checking for the modifier.
        
        Returns:
            List: A list of ModEffect namedtutples with the modifier, time effect and cost effect
        """
        return self._bloodlines

    def global_modifiers(self):
        """Returns a list of tuples indicating what modifiers globally affect
        build time or build cost and by how much.

        Used by templates to build blocks checking for the modifier.
        
        Returns:
            List: A list of ModEffect namedtutples with the modifier, time effect and cost effect
        """
        return self._global_mofidiers

    def event_modifiers(self):
        """Returns a list of tuples indicating what modifiers affect
        build time or build cost and by how much.

        Used by templates to build blocks checking for the modifier.
        
        Returns:
            List: A list of ModEffect namedtutples with the modifier, time effect and cost effect
        """
        return self._modifiers

    def technology(self):
        """Returns a list of tuples indicating what technology affect
        build time or build cost and by how much.

        Used by templates to build blocks checking for the technology.
        
        Returns:
            List: A list of TechEffect namedtutples with the tech, time effect and cost effect
        """
        return self._technology

    def job_actions(self):
        """Returns a list of tuples indicating what job_action and attributes 
        affect build time and by how much.

        Used by templates to build blocks checking for the job action.

        I should consider rewriting this so I can iterate over all 
            costs and times.            
        
        Returns:
            List: A list of JobActionEffect named tuples with job action, attributes,
             and their effect on build time / build cost.
        """
        return self._job_actions
