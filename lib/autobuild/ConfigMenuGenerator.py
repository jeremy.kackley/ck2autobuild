# -*- coding: utf-8 -*-
import operator

from ck2.code.base import Decision, assign, make_b, make_if, make_if_not
from ck2.code.cond import has_character_flag
from ck2.modbase import Localization
from defines import (ALLVASSAL_FLAG, AUTOBUILD_ENABLED_FLAG,
                     AUTOBUILD_MENU_FLAG, COUNTYBARON_FLAG,
                     GOLD_RESERVE_POINTS, PRESTIGE_RESERVE_POINTS,ABCF_INIT,
                     VASSALBARON_FLAG)
from localization_strings import AB_LS
import common.effects as effects


class ConfigMenuGenerator:
    """
    This will eventually generate the (more or less static)
        open/close/start/stop and configuration events.
    """

    def __init__(self, parent):
        self._menuflag = AUTOBUILD_MENU_FLAG
        self._enabled_flag = AUTOBUILD_ENABLED_FLAG
        self.parent = parent
        self.extra_check_block = ''
        # option to add debug options, similar to friend-check
        #    to display various test scopes to verify they are working?
        self._loca = {}
        self._created_items = []

    def _create_allvassal_toggle(self):
        self._create_toggle_in_menu(ALLVASSAL_FLAG, 'enable_' + ALLVASSAL_FLAG,
                                    'disable_' + ALLVASSAL_FLAG)
        self._loca['enable_' + ALLVASSAL_FLAG] = ('ENABLE_ALLVASSAL_MASK', )
        self._loca['disable_' + ALLVASSAL_FLAG] = ('DISABLE_ALLVASSAL_MASK', )
        self._loca['enable_' + ALLVASSAL_FLAG + '_desc'] = (
            'ENABLE_ALLVASSAL_DESC_MASK', )
        self._loca['disable_' + ALLVASSAL_FLAG + '_desc'] = (
            'DISABLE_ALLVASSAL_DESC_MASK', )

    def _create_countybaron_toggle(self):
        self._create_toggle_in_menu(COUNTYBARON_FLAG,
                                    'enable_' + COUNTYBARON_FLAG,
                                    'disable_' + COUNTYBARON_FLAG)
        self._loca['enable_' + COUNTYBARON_FLAG] = (
            'ENABLE_COUNTYBARON_MASK', )
        self._loca['disable_' + COUNTYBARON_FLAG] = (
            'DISABLE_COUNTYBARON_MASK', )
        self._loca['enable_' + COUNTYBARON_FLAG + '_desc'] = (
            'ENABLE_COUNTYBARON_DESC_MASK', )
        self._loca['disable_' + COUNTYBARON_FLAG + '_desc'] = (
            'DISABLE_COUNTYBARON_DESC_MASK', )

    def _create_vassalbaron_toggle(self):
        self._create_toggle_in_menu(VASSALBARON_FLAG,
                                    'enable_' + VASSALBARON_FLAG,
                                    'disable_' + VASSALBARON_FLAG)
        self._loca['enable_' + VASSALBARON_FLAG] = (
            'ENABLE_VASSALBARON_MASK', )
        self._loca['disable_' + VASSALBARON_FLAG] = (
            'DISABLE_VASSALBARON_MASK', )
        self._loca['enable_' + VASSALBARON_FLAG + '_desc'] = (
            'ENABLE_VASSALBARON_DESC_MASK', )
        self._loca['disable_' + VASSALBARON_FLAG + '_desc'] = (
            'DISABLE_VASSALBARON_DESC_MASK', )

    def _create_prestige_reserves(self, flag, amnt, other, otherflag):
        other = [
            assign('clr_character_flag', otherflag)
            for otherflag in PRESTIGE_RESERVE_POINTS if otherflag != flag
        ]
        other = '\n'.join(other)
        self._create_toggle_in_menu(flag, 'enable_' + flag, 'disable_' + flag,
                                    other)
        self._loca['enable_' + flag] = ('ENABLE_PRESTIGE_MASK', amnt)
        self._loca['disable_' + flag] = ('DISABLE_PRESTIGE_MASK', amnt)
        self._loca['enable_' + flag + '_desc'] = ('ENABLE_PRESTIGE_DESC_MASK',
                                                  amnt)
        self._loca['disable_' + flag + '_desc'] = (
            'DISABLE_PRESTIGE_DESC_MASK', amnt)
        return other, otherflag

    def _create_gold_reserve(self, flag, amnt):
        other = [
            assign('clr_character_flag', otherflag)
            for otherflag in GOLD_RESERVE_POINTS if otherflag != flag
        ]
        other = '\n'.join(other)
        self._create_toggle_in_menu(flag, 'enable_' + flag, 'disable_' + flag,
                                    other)
        self._loca['enable_' + flag] = ('ENABLE_GOLD_MASK', amnt)
        self._loca['disable_' + flag] = ('DISABLE_GOLD_MASK', amnt)
        self._loca['enable_' + flag + '_desc'] = ('ENABLE_GOLD_DESC_MASK',
                                                  amnt)
        self._loca['disable_' + flag + '_desc'] = ('DISABLE_GOLD_DESC_MASK',
                                                   amnt)
        return other, otherflag

    def add_reserves(self):
        # think i can use _create_toggle_in_menu to
        #    hide prestige / gold options
        self._create_vassalbaron_toggle()
        self._create_countybaron_toggle()
        self._create_allvassal_toggle()
        for flag, amnt in sorted(
                GOLD_RESERVE_POINTS.items(), key=operator.itemgetter(1)):
            other, otherflag = self._create_gold_reserve(flag, amnt)
        for flag, amnt in sorted(
                PRESTIGE_RESERVE_POINTS.items(), key=operator.itemgetter(1)):
            self._create_prestige_reserves(flag, amnt, other, otherflag)

    def _add_loca(self, name, mask):
        self._loca[name] = (mask, )
        self._add_loca_desc(name, mask)

    def _add_loca_desc(self, name, mask):
        self._loca[name + '_desc'] = (mask, )

    def create_all(self):
        self._created_items = []
        self._create_toggle(self._menuflag, 'open_autobuild_menu',
                            'close_autobuild_menu')
        self._add_loca('open_autobuild_menu', 'OPEN_AB_MENU_MASK')
        self._add_loca('close_autobuild_menu', 'CLOSE_AB_MENU_MASK')

        clear_flags = effects.clr_all_autobuilding_flags()+effects.clr_province_tech_level_flags()

        other_eff = make_if(
            has_character_flag(self._enabled_flag),
            self.control.call_event(ABCF_INIT))
        other_eff += make_if_not(
            has_character_flag(self._enabled_flag),
            clear_flags)

        self._create_toggle_in_menu(self._enabled_flag, 'enable_autobuilding',
                                    'disable_autobuilding', other_eff)
        self._add_loca('enable_autobuilding', 'ENABLE_AB_MASK')
        self._add_loca('disable_autobuilding', 'DISABLE_AB_MASK')

        self.add_reserves()
        return self._created_items

    def _create_toggle_in_menu(self, flag, on_name, off_name, other_effect=''):
        in_menu = assign('has_character_flag', self._menuflag)
        self._create_toggle(flag, on_name, off_name, other_effect, in_menu)

    def _create_toggle(self,
                       flag,
                       on_name,
                       off_name,
                       other_effect='',
                       other_condition=''):

        noai = assign('ai', 'no')
        ruler = assign('is_ruler', 'yes')
        isopen = assign('has_character_flag', flag)
        isnotopen = make_b('NOT', isopen) + self.extra_check_block

        on = Decision(on_name)
        on.is_high_prio = 'no'
        on.ai_will_do = assign('factor', 0)
        on.potential = noai + ruler + isnotopen + other_condition
        on.effect = assign('set_character_flag', flag) + other_effect

        off = Decision(off_name)
        off.is_high_prio = 'no'
        off.ai_will_do = assign('factor', 0)
        off.potential = noai + ruler + isopen + other_condition
        off.effect = assign('clr_character_flag', flag) + other_effect

        self._created_items.append(on)
        self._created_items.append(off)

    def gen_loca(self):
        ret = []
        for name in sorted(self._loca):
            maskname = self._loca[name][0]
            new = Localization(name)
            new.filename = 'config_decisions.csv'
            for mask, lang in new.walk(AB_LS[maskname]):
                if len(self._loca[name]) > 1:
                    #the amounts have an argument.
                    mask = mask.format(self._loca[name][1])
                new.add(mask, lang)
            ret.append(new)
        return ret
