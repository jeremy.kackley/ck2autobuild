from os.path import abspath, dirname, join

from jinja2 import Environment, FileSystemLoader, Template

from ck2.code.base import make_event_target
from ck2.modbase import Localization
from Pay import Pay

from .. import defines
from ..localization_strings import AB_LS

env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')))

class Refund(Pay):
    '''
    Triggered on target province; charges payee based on province
    flags.
    '''

    def __init__(self, namespace='autobuild_refund',discount_calc=None):
        Pay.__init__(self, namespace)
        self._pretty_name = "RefundEvents"
        self.discount_calc=discount_calc #set by manager

    def _immediate(self, building):
        template = env.get_template('scaled_refund.j2')
        ret = template.render(
            breakpoints=self.discount_calc.cost_breakpoints(building.build_cost_int()),
            target=defines.ABET_PAYEE)
        return ret

    def _set_event_specifics(self, new_event, building):
        new_event.notification = 'yes'
        new_event.comment = "refund {}".format(building.build_cost_int())
        if defines.DEBUG_CONTROLFLOW:
            new_event.log = 'AB Refunding some portion of {} based on conditions in [This.GetName]'.format(
                building.build_cost())
        return new_event

    def _link(self, building):
        building.refund_event = self._blocks[building.build_cost_int()].id

    # these are worth localizing.
    def gen_loca(self):
        ret = []
        for name in sorted(self._loca):
            new = Localization(name)
            new.filename = 'refund_events.csv'
            for mask, lang in new.walk(AB_LS['REFUND_MASK']):
                new.add(
                    mask.format(self._loca[name].build_cost(),
                                self._loca[name].name_l(lang)), lang)
            ret.append(new)
        return ret
