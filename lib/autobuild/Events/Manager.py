from datetime import datetime

from ck2.modbase import ModBase, gen_readme_report
from Control import Control
from FinishBuild import FinishBuild
from Pay import Pay
from Refund import Refund
from Select import Select
from StartBuild import StartBuild


class Manager(ModBase):
    def __init__(self, parent):
        ModBase.__init__(self)
        self.parent = parent
        self.filename_mask = parent.filename_mask
        self.bm = parent.bm
        parent.submods.append(self)
        self.pay = Pay(discount_calc=parent.dc)
        self.refund = Refund(discount_calc=parent.dc)
        self.start = StartBuild(discount_calc=parent.dc)
        self.finish = FinishBuild()
        self.control = Control()
        self.start.control = self.control
        # everything involving select is pending a rewrite.
        self.select = Select(self)
        # all these links, or at least most of them, are
        # probably transitory.
        self.finish.selection_generator = self.select
        self.control.select = self.select
        self.select.control = self.control
        self.finish.control = self.control
        self.select.config_menu = parent.config
        parent.config.control = self.control

    @gen_readme_report('event scripted effects')
    def _gen_script_effects(self):
        count = 0
        start = datetime.now()
        modules_with_se = [
            self.finish,
            self.control,
        ]
        fn = 'event.txt'
        for module in modules_with_se:
            for x in module.se.get_items_by_category():
                self.scripted_effects.add(x, fn)
                count += 1
        return count

    @gen_readme_report('start events')
    def _gen_start_events(self):
        count = 0
        for building in self.bm.sets.get_buildings_sorted_by_type():
            fn = 'start_' + building.type() + '.txt'
            self.events.add(self.start.create(building), fn)
            count += 1
        return count

    @gen_readme_report('finish events')
    def _gen_finish_events(self):
        count = 0
        for building in self.bm.sets.get_buildings_sorted_by_type():
            fn = 'finish_' + building.type() + '.txt'
            self.events.add(self.finish.create(building), fn)
            count += 1
        return count

    @gen_readme_report('pay events')
    def _gen_pay_events(self):
        for building in self.bm.sets.get_buildings_sorted_by_gold():
            self.pay.create(building)
        fn = 'pay_events.txt'
        count = 0
        for event in self.pay.get_created_events():
            count += 1
            self.events.add(event, fn)
        return count

    @gen_readme_report('refund events')
    def _gen_refund_events(self):
        for building in self.bm.sets.get_buildings_sorted_by_gold():
            self.refund.create(building)
        fn = 'refund_events.txt'
        count = 0
        for event in self.refund.get_created_events():
            count += 1
            self.events.add(event, fn)
        return count

    @gen_readme_report('select event')
    def _gen_select_event(self):
        # this will be rewritten at some point...
        new = self.select.create(self.bm.sets)
        self.events.add(new, 'select_event.txt')

    @gen_readme_report('control events')
    def _gen_abcf(self):
        count = 0
        fn = 'control_events.txt'
        for event in self.control.create():
            self.events.add(event, fn)
            count += 1
        return count

    @gen_readme_report('on action')
    def _gen_onactions(self):
        count=0
        for thing in self.control.create_on_action():
            self.on_actions.add(thing)
            count+=1
        return count

    @gen_readme_report('event localizations')
    def _gen_loca(self):
        count = 0
        for thing in (
                self.start,
                self.finish,
                self.select,
                self.refund,
                self.control, ):
            for loc in thing.gen_loca():
                self.localizations.add(loc)
                count += 1
        return count

    @gen_readme_report('all event related things')
    def generate(self):
        self.clear()
        # must be called first, because start events call them.
        self._gen_refund_events()
        self._gen_finish_events()
        self._gen_pay_events()
        self._gen_start_events()
        self._gen_select_event()
        self._gen_abcf()
        self._gen_onactions()
        self._gen_loca()
        self._gen_script_effects()
