# -*- coding: utf-8 -*-
from ck2.code.base import CharEvent, make_b, assign, make_if
from ck2.code.cmd import clr_character_flag, set_character_flag
from ck2.modbase import Localization

from ..common.effects import autobuild_select_building
from ..common.triggers import autobuild_not_selected_building
from ..localization_strings import AB_LS
from ..defines import ABCF_BUILT_FLAG, ABCF_REPEAT_FLAG, ABCF_REACT_FLAG, DEBUG_CONTROLFLOW


class Select:
    # I might subclass this later, but for now, it works

    def __init__(self, parent):
        self.parent = parent
        self.namespace = "auto_build_selection_events"
        self.config_menu = None
        self._pretty_name = "Selection Event"
        # temporarily.
        self.selected_building_flag = ABCF_BUILT_FLAG
        self.selection_enqued_flag = ABCF_REPEAT_FLAG
        self.selection_reactive_flag = ABCF_REACT_FLAG
        self._loca = {}
        self.created = 1

    def selector_event(self):
        return '{}.1'.format(self.namespace)

    def trigger_initialization(self, days=1):
        # this is a speical event that:
        #    checks all demense/vassal prov
        #        for:
        #            faux building
        #            no modifier
        #    and if it finds them; it
        #        replaces them with the real
        #        building.
        #    localization can be
        #        "Your advisors check for badness?  lol
        pass

    def trigger_selection(self, days=1):
        sid = assign('id', self.selector_event())
        effect = make_b('character_event', sid + assign('days', days))
        effect += assign('set_character_flag', self.selection_reactive_flag)
        limit = self.not_reactive_selection_queued()
        return make_if(limit, effect)

    def start_selection(self):
        return assign('clr_character_flag', self.selected_building_flag)

    def make_selection(self):
        return assign('set_character_flag', self.selected_building_flag)

    def made_selection(self):
        return assign('has_character_flag', self.selected_building_flag)

    def not_made_selection(self):
        return make_b('not', self.made_selection())

    def selection_enqued(self):
        return assign('has_character_flag', self.selection_enqued_flag)

    def not_selection_enqued(self):
        return make_b('NOT', self.selection_enqued())

    def reactive_selection_queued(self):
        return assign('has_character_flag', self.selection_reactive_flag)

    def not_reactive_selection_queued(self):
        return make_b('NOT', self.reactive_selection_queued())

    def _do_stuff_block(self, bldg):
        ret = bldg.commands.call_start_event()
        reno_blk = bldg.commands.start_renovating()

        if not bldg.is_extra_holding_type():
            # extra holding types should already be in
            # the province scope.
            ret = reno_blk + make_b('location', ret)
        else:
            ret = reno_blk + ret

        ret = autobuild_select_building() + ret
        # changed to global so I can make it a script_trigger.
        already_selected = autobuild_not_selected_building()
        # exception for shipyards
        if bldg.requires_port():
            port = assign('port', 'yes')
            if not bldg.is_extra_holding_type():
                port = make_b('location', port)
            already_select = already_selected + port
        ret = ret + assign('break', 'yes')
        return make_if(already_selected, ret)

    def _gen_requeue_block(self):
        nothasflag = self.not_selection_enqued()
        enabled = assign('has_character_flag', self.config_menu._enabled_flag)
        setflag = assign('set_character_flag', self.selection_enqued_flag)
        myid = assign('id', self.selector_event())
        nextevent = make_b('character_event', myid + assign('days', 120))
        # go ahead and enque the next 'periodic' event
        default = make_if(nothasflag + enabled, setflag + nextevent)
        # if I successfully made a seleciton, go ahead and trigger another
        #    reative event.
        #        Note that I should consider 0 day event, but capping
        #        the number of triggers...e.g. count < 50 or something.
        default += make_if(self.made_selection(), self.trigger_selection(1))
        return make_b('ROOT', default)

    def _reset_flag_if_older_than(self, flag, days):
        flag_str = assign('flag', flag)
        days_str = assign('days', days)
        had_flag = make_b('had_character_flag', flag_str + days_str)
        clr_flag = assign('clr_character_flag', flag)
        return make_if(had_flag, clr_flag)

    def _immediate(self, sets):
        # get rid of all possible flags that could have gotten us here.
        #other = self._reset_flag_if_older_than(self.selection_enqued_flag, 100)
        #other += assign('clr_character_flag', self.selection_reactive_flag)
        #other += self.start_selection()

        #default = self._gen_requeue_block()
        # default should be (for now):
        #   clear reacting/doing something flag
        #   repeat
        #default = clr_character_flag(ABCF_SELECTING_FLAG)
        #default+= clr_character_flag(ABCF_REACT_FLAG)
        #default+= self.control.call_event(ABCF_REPEAT)

        # sets allows for more efficient nesting.
        select = sets.select(callback=self._do_stuff_block)
        select = make_if(
            assign('has_character_flag', self.config_menu._enabled_flag),
            select)
        return select + self.control.call_payee_repeat()

    def create(self, sets):
        new_evnt = CharEvent(
            self.created, picture='GFX_evt_council', namespace=self.namespace)
        self.created += 1
        self._loca[new_evnt.desc] = ''
        # no reason to spam the user with
        #    'we are thinking about building stuff' spam
        new_evnt.hide_window = 'yes'
        new_evnt.is_triggered_only = 'yes'
        new_evnt.immediate = self._immediate(sets)
        if DEBUG_CONTROLFLOW:
            new_evnt.log = 'AB Starting build selection on [This.GetBestName].'

        return new_evnt

    def gen_loca(self):
        ret = []
        for name in sorted(self._loca):
            building = self._loca[name]
            new = Localization(name)
            new.filename = 'selection_events.csv'
            for mask, lang in new.walk(AB_LS['FINISH_MASK']):
                new.add(mask, lang)
            ret.append(new)
        return ret
