#-*- coding: utf-8 -*-
from ck2.code.base import ProvEvent, ScriptedEffects, make_b
from ck2.modbase import Localization
from ck2.code.base import make_event_target, assign, make_if
from ..localization_strings import AB_LS
from ..common.functions import get_has_building_variable_from_type
from .. import defines

class FinishBuild:
    '''
    Triggered once building is completed, handles 'cleanup.'
    '''

    def __init__(self, namespace='autobuild_finishbuilding'):
        self.namespace = namespace
        self._loca = {}
        self._pretty_name = "FinishBuilding Events"
        self.created = 1
        self.se = ScriptedEffects()

    def _criteria(self, building):
        contents = assign('holding_type', building.type())
        return contents

    def _immediate(self, building):
        effect = building.commands.stop_renovating()
        effect += self.control.call_payee_react()
        dothings = building.add_block()
        refund = ''
        rfe = ''
        if building.build_cost_is_positive_integer():
            sename = 'ab_refund_{:04d}'.format(building.build_cost_int())
            if building.is_extra_holding_type():
                sename = sename + '_eht'
            if not self.se.exists(sename):
                rfe = building.commands.call_refund_event()
                if not building.is_extra_holding_type():
                    rfe = make_event_target(defines.ABET_LOCATION, rfe)
                self.se.add(sename, rfe, 'refund')
            rfe = self.se.call(sename)

        if building.prestige_cost():
            refund = 'prestige = {}'.format(building.prestige_cost())
            refund = make_event_target(defines.ABET_PAYEE, refund)
        if building.upgrades_from() and building.replaces():
            temp = building.remove_precursor_block()
        hba = get_has_building_variable_from_type(building.type())
        already_exists = assign(hba, building.name())
        docheck = dothings
        undocheck = ''
        if refund + rfe != '':
            undocheck = make_if(already_exists,
                                refund + rfe + assign('break', 'yes'))
            effect = effect + undocheck + docheck
        else:
            effect += dothings
        title_limits = building.conditions.renovating()
        limit = make_b('limit', title_limits)

        # scope should be any_province_holding unless extra holding type
        if building.type() == 'family_palace':
            # proper scope is ABET_OWNER = { family_palace = {}}
            effect = make_event_target(defines.ABET_OWNER,
                                       make_b('family_palace', effect))
        elif not building.is_extra_holding_type():
            # proper scope is any_province_holding
            effect = make_b('any_province_holding', limit + effect)
        else:
            # else, extra holding types are already in the right scope,
            #   but should check title limits.
            effect = make_b('if', limit + effect)
        return effect

    def create(self, building):
        # setup new event
        new_event = ProvEvent(self.created, namespace=self.namespace)
        new_event.is_triggered_only = 'yes'
        new_event.notification = 'yes'
        new_event.immediate = self._immediate(building)
        new_event.comment = "finish {}".format(building.name())
        if defines.DEBUG_CONTROLFLOW:
            new_event.log = 'AB Finished building {} in [This.GetName]'.format(
                building.name())
        # housekeeping
        self.created += 1
        self._loca[new_event.desc] = building
        # linking
        building.finish_event = new_event.id
        return new_event

    def gen_loca(self):
        ret = []
        for name in sorted(self._loca):
            building = self._loca[name]
            new = Localization(name)
            new.filename = 'finish_events_{}.csv'.format(building.type())
            for mask, lang in new.walk(AB_LS['FINISH_MASK']):
                string = mask.format(building.name_l(lang))
                new.add(string, lang)
            ret.append(new)
        return ret
