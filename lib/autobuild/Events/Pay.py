from collections import OrderedDict

from ck2.code.base import ProvEvent
from ck2.modbase import Localization
from ck2.code.base import make_event_target
from .. import defines
from ..localization_strings import AB_LS

from os.path import abspath, dirname, join

from jinja2 import Environment, FileSystemLoader, Template

env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')))

class Pay:
    '''
    Triggered on target province; charges payee based on province
    flags.
    '''

    def __init__(self, namespace='autobuild_pay',discount_calc=None):
        self.namespace = namespace
        self._loca = {}
        self._pretty_name = "PayEvents"
        self.created = 1
        self._blocks = OrderedDict()
        self.discount_calc=discount_calc #set by manager

    def _immediate(self, building):
        template = env.get_template('scaled_pay.j2')
        ret = template.render(
            breakpoints=self.discount_calc.cost_breakpoints(building.build_cost_int()),
            target=defines.ABET_PAYEE)
        return ret

    def _set_event_specifics(self, new_event, building):
        new_event.hide_window = 'yes'
        new_event.comment = "pay {}".format(building.build_cost_int())
        if defines.DEBUG_CONTROLFLOW:
            new_event.log = 'AB Paying some portion of {} based on conditions in [This.GetName]'.format(
                building.build_cost())
        return new_event

    def create(self, building):
        building.pay_event=None
        if building.build_cost_int() and building.build_cost_int()!=0:
            if not self._blocks.has_key(building.build_cost_int()):
                # setup new event
                new_event = ProvEvent(self.created, namespace=self.namespace)
                new_event.is_triggered_only = 'yes'
                new_event = self._set_event_specifics(new_event, building)
                # payments are very routine, ergo hidden.
                new_event.immediate = self._immediate(building)
                # housekeeping
                self.created += 1
                self._loca[new_event.desc] = building
                self._blocks[building.build_cost_int()] = new_event
            # linking
            self._link(building)

    def _link(self, building):
        building.pay_event = self._blocks[building.build_cost_int()].id

    def get_created_events(self):
        return self._blocks.values()

    # I didn't bother localizaing this file
