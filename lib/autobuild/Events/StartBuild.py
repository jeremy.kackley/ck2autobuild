# -*- coding: utf-8 -*-
from os.path import abspath, dirname, join

from jinja2 import Environment, FileSystemLoader, Template

from ck2.code.base import ProvEvent, make_b, make_event_target
from ck2.code.cmd import clr_character_flag
from ck2.modbase import Localization

from .. import defines
from ..localization_strings import AB_LS

env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')))

class StartBuild:
    '''
    Triggered on target province; sets up modifier, triggers payment, and
    schedules finish event.
    '''

    def __init__(self, namespace='autobuild_startbuilding',discount_calc=None):
        self.namespace = namespace
        self._loca = {}
        self._pretty_name = "StartBuilding Events"
        self.created = 1
        self.discount_calc=discount_calc #set by manager

    def initialize_key(self, namespace, string):
        if namespace not in self._name_index:
            self._name_index[namespace] = {}
            self._is_sorted[namespace] = False
        self._name_index[namespace][string]

    def _immediate(self, bldg):        
        scope = 'any_province_holding'
        if bldg.is_extra_holding_type():
            scope = ''
        prov_has_fake = bldg.conditions.renovating()
        if scope:
            if bldg.type()=='family_palace':
                prov_has_fake = make_event_target(defines.ABET_OWNER, make_b('family_palace', prov_has_fake))
            else:
                prov_has_fake = make_b(scope, prov_has_fake)
        template = env.get_template('startbuilding.j2')
        ret = template.render(
            breakpoints=self.discount_calc.time_breakpoints(bldg.build_time_int()),
            repeat_event=self.control.call_payee_repeat(),
            has_building=prov_has_fake,
            building=bldg,
            payee=defines.ABET_PAYEE,
            debug=defines.DEBUG_START_BUILDING)
        return ret

    def create(self, building):
        # setup new event
        new_event = ProvEvent(self.created, namespace=self.namespace)
        new_event.is_triggered_only = 'yes'
        new_event.notification = 'yes'
        new_event.immediate = self._immediate(building)
        new_event.comment = "start {}".format(building.name())
        if defines.DEBUG_CONTROLFLOW:
            new_event.log = 'AB Started building {} in [This.GetName]'.format(
                building.name())
        # housekeeping
        self.created += 1
        self._loca[new_event.desc] = building
        # linking
        building.start_event = new_event.id
        return new_event

    def gen_loca(self):
        ret = []
        for name in sorted(self._loca):
            new = Localization(name)
            new.filename = 'start_events_{}.csv'.format(self._loca[name].type(
            ))
            for mask, lang in new.walk(AB_LS['START_MASK']):
                new.add(mask.format(self._loca[name].name_l(lang)), lang)
            ret.append(new)
        return ret
