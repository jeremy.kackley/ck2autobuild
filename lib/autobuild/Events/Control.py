from collections import OrderedDict
from os.path import abspath, dirname, join

from jinja2 import Environment, FileSystemLoader, Template

from ck2.code.base import (CharEvent, ProvEvent, ScriptedEffects, assign, log,
                           make_event_target, make_if, make_if_not,
                           make_scripted_object, set_event_target)
from ck2.code.cmd import (character_event, clr_character_flag, province_event,
                          set_character_flag)
from ck2.code.cond import had_character_flag, has_character_flag, is_alive
from ck2.modbase import Localization
from ck2.on_actions import On_Actions_Events

from ..common.effects import (clr_all_autobuilding_flags,
                              clr_province_tech_level_flags)
from ..common.functions import repeat_event
from ..localization_strings import AB_LS
from .. import defines

#while I could make these methods, I created them
#   because it makes the below code
#   somewhat more interpretable.


def make_if_need_to_react(something):
    #not doing something
    #not reacting
    #built something
    not_flags = ''
    for flag in [
            defines.ABCF_SELECTING_FLAG,
            defines.ABCF_REACT_FLAG,
    ]:
        not_flags += has_character_flag(flag)
    if_built = make_if(has_character_flag(defines.ABCF_BUILT_FLAG), something)
    ret = make_if_not(not_flags, if_built)
    return ret


env = Environment(
    loader=FileSystemLoader(join(abspath(dirname(__file__)), 'templates')))


class Control:
    '''
    Events that handle branching and control flow;
    Planned events in this "scope":
        Init Event = Set Payee, call Start if 
        Start_Event = sets start_selection flag, triggers Effective_balance
        repeat = event to schedule a start repeat if not already requeued and if not still 'in it'
        on_death = if flagged character dies, backup flags to dynasty, broadcast restart to human players
        restart = if human players from dynasty, copy flags, call kickoff,

    Unplanned, but potentially useful on_action events:
        Yearly Pulse = on a yearly basis, restart event chain to prevent stack overflow
        on_bi_yearly_pulse
        on_five_year_pulse
        on_decade_pulse
        on_settlement_construction_completed
        on_trade_post_construction_completed
        on_holding_building_start

    might need to use repeat_event to prevent stack overflows.
    '''

    def __init__(self, namespace='autobuild_control_events'):
        self.namespace = namespace
        self._loca = {}
        self._pretty_name = "Control Events"
        self.created = 0
        self.events = OrderedDict()
        self._localized_events = [
            defines.ABCF_INIT,
            defines.ABCF_ON_DEATH,
            defines.ABCF_RESTART,
        ]
        #for now this works, maybe move it to a define later.     
        #if DEBUG_CONTROLFLOW:
        #add all control evens to localization
        #   so we can see them fire.
        #self._localized_events.append(ABCF_REPEAT)
        #self._localized_events.append(ABCF_REACT)
        #self._localized_events.append(ABCF_START)
        for thing in [
                defines.ABCF_INIT,
                defines.ABCF_START,
                defines.ABCF_REPEAT,
                defines.ABCF_REACT,
                defines.ABCF_ON_DEATH,
                defines.ABCF_RESTART,
                defines.ABCF_YEARLY_PULSE,
        ]:
            new_event = CharEvent(self.created, namespace=self.namespace)
            new_event.is_triggered_only = 'yes'
            if thing in self._localized_events:
                new_event.notification = 'yes'
            else:
                new_event.hide_window = 'yes'
            new_event.comment = thing
            if defines.DEBUG_CONTROLFLOW:
                new_event.log = 'AB called on [This.GetBestName] {}.'.format(
                    thing)
            self.events[thing] = new_event
            self.created += 1
        #this class requires these things be set externally
        #self.select=None            
        self.se = ScriptedEffects()

    def _abort(self):
        #not sure if this is valid, but
        return make_if_not(is_alive('yes'), assign('break', 'yes'))

    def _add_effect(self, event_string, effect):
        #only do anything if It's enabled, otherwise...
        effect = make_if(has_character_flag(defines.AUTOBUILD_ENABLED_FLAG), effect)
        effect += make_if_not(
            has_character_flag(defines.AUTOBUILD_ENABLED_FLAG),
            clr_all_autobuilding_flags()+clr_province_tech_level_flags())
        self.events[event_string].immediate = self._abort() + effect
        #self.events[event_string].add_option('"ACCEPT"',self._abort()+effect)

    def _create_init(self):
        'initializes auto tag; not generally repeated'
        lm = ''
        if defines.DEBUG_CONTROLFLOW:
            lm = log('init',
                     'setting PAYEE,set to [{}.GetID]'.format(defines.ABET_PAYEE))
        template = env.get_template('init.j2')
        flagslist=[defines.ABCF_REACT_FLAG, defines.ABCF_REPEAT_FLAG, defines.ABCF_BUILT_FLAG,
                defines.ABCF_RCS_FLAG, defines.ABCF_SELECTING_FLAG]
        ret = template.render(
            enabled_flag=defines.AUTOBUILD_ENABLED_FLAG,
            set_event_target=set_event_target(defines.ABET_PAYEE) + lm,
            flags=flagslist,
            start_event=self.events[defines.ABCF_START].id)
        self._add_effect(defines.ABCF_INIT, ret)

    def _create_start(self):
        'this is the event that should be repeated'
        #in theory, if this calls select.start, I'm in business.
        #set "I'm doing stuff" flag
        #if repeat called start, clear repeat enqueued flag so it can
        #   be reenqued        
        #clear "reacting" flag
        #clear "built something" flag
        lm = ''
        #clear repeat if necessary
        if defines.DEBUG_CONTROLFLOW:
            lm = log('start', 'clearing repeat flags')
        ret = set_character_flag(defines.ABCF_SELECTING_FLAG)
        clr_rep_flags = clr_character_flag(defines.ABCF_REPEAT_FLAG)
        clr_rep_flags += clr_character_flag(defines.ABCF_RCS_FLAG)
        ret += make_if(has_character_flag(defines.ABCF_RCS_FLAG), lm + clr_rep_flags)

        #clear reacting
        ret += clr_character_flag(defines.ABCF_REACT_FLAG)
        ret += clr_character_flag(defines.ABCF_BUILT_FLAG)

        #call select
        if defines.DEBUG_CONTROLFLOW:
            lm = log('start', 'calling select')
        aid = self.select.selector_event()
        ret = ret + lm + character_event(aid)
        self._add_effect(defines.ABCF_START, ret)

    def _create_react(self):
        #if I'm not already reacting or doing stuff, react, otherwise
        #   do nothing.
        lm = ''
        if defines.DEBUG_CONTROLFLOW:
            lm = log('react', 'calling start')
        react = lm + repeat_event(self.events[defines.ABCF_START].id, 1)
        reacting_or_doing_stuff = has_character_flag(defines.ABCF_REACT_FLAG)
        reacting_or_doing_stuff += has_character_flag(defines.ABCF_SELECTING_FLAG)
        ret = make_if_not(reacting_or_doing_stuff,
                          set_character_flag(defines.ABCF_REACT_FLAG) + react)
        #this, these all, are called from character
        #ret=make_event_target(ABET_PAYEE,ret)
        self._add_effect(defines.ABCF_REACT, ret)

    def _create_repeat(self):
        #called at natural end,
        #   if i need to react, react, done
        #   if not ABCF_REPEAT_FLAG, I'm not pusling,
        #       set and start pulsing, done
        #   if had ABCF_REPEAT_FLAG for 100 days
        #       set 'repeating start' flag
        #       repeat start
        #       done
        lm = ''
        done = assign('break', 'yes')

        #handle need to react
        if defines.DEBUG_CONTROLFLOW:
            lm = log('repeat', 'calling react')
        react = self.call_event(defines.ABCF_REACT, 1)
        ret = make_if_need_to_react(react + done)

        #handle need to start pulsing
        if defines.DEBUG_CONTROLFLOW:
            lm = log('repeat', 'repeating self in 30 days')
        pulse = lm + repeat_event(self.events[defines.ABCF_REPEAT].id, 30)
        ret += make_if_not(
            has_character_flag(defines.ABCF_REPEAT_FLAG),
            set_character_flag(defines.ABCF_REPEAT_FLAG) + pulse + done)

        #handle repeat start
        if defines.DEBUG_CONTROLFLOW:
            lm = log('repeat', 'repeating start because 100 days elapsed.')
        had_flag = had_character_flag(defines.ABCF_REPEAT_FLAG, 100)
        repeat_start = set_character_flag(defines.ABCF_RCS_FLAG)
        repeat_start += lm + repeat_event(self.events[defines.ABCF_START].id)
        ret += make_if(had_flag, repeat_start + done)
        self._add_effect(defines.ABCF_REPEAT, ret)

    def _create_on_death(self):
        template = env.get_template('on_death.j2')
        ret = template.render(
            enabled_flag=defines.AUTOBUILD_ENABLED_FLAG,
            flags=defines.ALL_SETTINGS_FLAGS,
            restart_event=self.events[defines.ABCF_RESTART].id)        
        self.events[defines.ABCF_ON_DEATH].has_character_flag = defines.AUTOBUILD_ENABLED_FLAG
        self.events[defines.ABCF_ON_DEATH].immediate = ret

    def _create_restart(self):
        template = env.get_template('restart.j2')
        ret = template.render(
            enabled_flag=defines.AUTOBUILD_ENABLED_FLAG,
            flags=defines.ALL_SETTINGS_FLAGS,
            start_event=self.events[defines.ABCF_INIT].id)
        self.events[defines.ABCF_RESTART].immediate = ret
        
    def _create_yearly_pulse(self):
        template = env.get_template('yearly_pulse.j2')
        ret = template.render(
            stuck_flags=defines.ALL_CONTROL_FLAGS,
            repeat_event=self.events[defines.ABCF_REPEAT].id)
        self.events[defines.ABCF_YEARLY_PULSE].has_character_flag = defines.AUTOBUILD_ENABLED_FLAG
        self._add_effect(defines.ABCF_YEARLY_PULSE, ret)

    def create(self):
        self._create_init()
        self._create_start()
        self._create_react()
        self._create_repeat()
        self._create_on_death()
        self._create_restart()
        self._create_yearly_pulse()
        return self.events.values()

    def create_on_action(self):
        od=On_Actions_Events('on_death')
        od.add_event(self.events[defines.ABCF_ON_DEATH].id)
        oyp=On_Actions_Events('on_yearly_pulse')
        oyp.add_event(self.events[defines.ABCF_YEARLY_PULSE].id)
        return (od,oyp)

    def call_event(self, event_name, days='', tooltip='', random=''):
        aid = self.events[event_name].id
        ret = character_event(aid, days, tooltip, random)
        return ret

    def call_payee_repeat(self):
        if not self.se.exists('abcf_trigger_payee_repeat'):
            default = clr_character_flag(defines.ABCF_SELECTING_FLAG)
            default += clr_character_flag(defines.ABCF_REACT_FLAG)
            default += self.call_event(defines.ABCF_REPEAT)
            default = make_event_target(defines.ABET_PAYEE, default)
            self.se.add('ab_abcf_trigger_repeat', default, 'control')
        return self.se.call('ab_abcf_trigger_repeat')

    def call_payee_react(self):
        if not self.se.exists('abcf_trigger_payee_react'):
            default = self.call_event(defines.ABCF_REACT)
            default = make_event_target(defines.ABET_PAYEE, default)
            self.se.add('abcf_trigger_payee_react', default, 'control')
        return self.se.call('abcf_trigger_payee_react')

    def gen_loca(self):
        ret = []
        for name in self._localized_events:
            event = self.events[name]
            new = Localization(event.desc)
            new.filename = 'control_events.csv'
            for mask, lang in new.walk(AB_LS[name]):
                new.add(mask, lang)
            ret.append(new)
        return ret
