# this is a somewhat-temporary file to help transitioning buildingsets
# to generic.  Ultimately it will probably go away as things are rewritten.
# for now though, it extends the generic buildingsets.

# pylint:disable=import-error
from ck2.sets import BuildingSets
from ck2.code.base import assign, make_b, make_if, set_event_target
from common.functions import get_has_building_variable_from_type
from common.triggers import not_renovating_any, holding_is_valid_autobuild_target, has_no_camp
from common.effects import flag_prov_tech_levels
import defines

class BuildingSetsAB(BuildingSets):
    def __init__(self):
        BuildingSets.__init__(self)

        # how to figure out if
        #    AND/OR blocks are needed?
        self.notes_enabled = defines.DEBUG_SELECT_NOTES

        # these are set after each call to gen_type...  respectively
        self._cur_type = None
        self._gen_type_blocks = 0
        self._gen_potential_blocks = 0
        self._gen_prerequisites_blocks = 0
        self._gen_tech_blocks = 0
        self._gen_upgrade_blocks = 0
        self._gen_exclusive_blocks = 0
        self._leftovers_handled = 0        

    def _add_note(self, ret, note):
        if self.notes_enabled:
            return add_comment(ret, note)
        return ret

    def _make_block(self, ret, limit, effect):
        ret += make_if(limit, effect)
        return ret

    def select(self, callback=None):
        # TODO Convert/rewrite this as a template...for clarity?
        handled = set()
        self._callback = callback
        ret = ''
        fo_ret = ''
        ho_ret = ''
        tp_ret = ''
        fp_ret = ''
        # sort so that we try cheaper things first.
        for pcost in sorted(self.by_prestige):
            costret = ''
            foc_ret = ''
            hoc_ret = ''
            tpc_ret = ''
            fpc_ret = ''
            pbuildings = self.by_prestige[pcost]
            # sort so that we try cheaper things first.
            for acost in sorted(self.by_gold):
                buildings_to_do = pbuildings.intersection(self.by_gold[acost])
                if buildings_to_do:
                    fo_part, handled = self.gen_type_fort(handled,
                                                              buildings_to_do)
                    ho_part, handled = self.gen_type_hospital(handled,
                                                              buildings_to_do)
                    fp_part, handled = self.gen_type_fp(handled,
                                                        buildings_to_do)
                    tp_part, handled = self.gen_type_trade_post(
                        handled, buildings_to_do)
                    part, handled = self.gen_type(handled, buildings_to_do)
                    # this should generally never happen, but it might.
                    part, handled = self._handle_leftovers(
                        part, handled, buildings_to_do, self._gen_type_blocks)
                    inner_blocks = self._leftovers_handled + \
                        self._gen_type_blocks
                    if part != '' and not self._callback and inner_blocks > 1:
                        part = make_b('OR', part)
                    if acost:
                        gold_lim = make_b(
                            'ROOT', assign(defines.SEM_GRB_MASK.format(acost), 'yes'))
                        if part:
                            part = make_if(gold_lim, part)
                        if fo_part:
                            fo_part = make_if(gold_lim, fo_part)                            
                        if ho_part:
                            ho_part = make_if(gold_lim, ho_part)
                        if fp_part:
                            fp_part = make_if(gold_lim, fp_part)
                        if tp_part:
                            tp_part = make_if(gold_lim, tp_part)
                    foc_ret += fo_part
                    hoc_ret += ho_part
                    tpc_ret += fp_part
                    fpc_ret += tp_part
                    costret += part
            if pcost:
                pres_lim = make_b('ROOT',
                                  assign(defines.SEM_PRB_MASK.format(pcost), 'yes'))
                if costret:
                    costret = make_if(pres_lim, costret)
                if foc_ret:
                    foc_ret = make_if(pres_lim, foc_ret)
                if hoc_ret:
                    hoc_ret = make_if(pres_lim, hoc_ret)
                if tpc_ret:
                    tpc_ret = make_if(pres_lim, tpc_ret)
                if fpc_ret:
                    fpc_ret = make_if(pres_lim, fpc_ret)
            fo_ret += foc_ret
            ho_ret += hoc_ret
            tp_ret += fpc_ret
            fp_ret += tpc_ret
            ret += costret
        # this should be impossible, but it might happen
        for building in self.by_name:
            if building not in handled:
                ret += self.gen_building(building)
        if not self._callback:
            # wrap it all in an OR.
            ret = make_b('OR', ret)
        self._callback = None
        ret = self._make_title_selection_block(ret)
        #these checks are to prevent creating empty blocks.  
        #   in practical terms, all should geenrally be created except forts.
        if fo_ret:
            ret += self._make_tb_weird(fo_ret, 'fort')
        if ho_ret:
            ret += self._make_tb_weird(ho_ret, 'hospital')
        if tp_ret:
            ret += self._make_tb_weird(tp_ret, 'trade_post')
        if fp_ret:
            ret += self._make_tb_weird(fp_ret, 'family_palace')
        return ret

    def all_realm_titles(self, limit, part):
        ret = set_event_target(defines.ABET_HOLDING)+part
        valid_targets = make_b('limit', not_renovating_any()+holding_is_valid_autobuild_target())
        ret = make_b('any_province_holding', valid_targets + ret)
        ret = make_b('county', make_b('owner',
                                      set_event_target(defines.ABET_OWNER))) + ret
        ret = flag_prov_tech_levels() + ret
        return make_b('any_realm_province', set_event_target(defines.ABET_LOCATION)+ret)

    def _make_title_selection_block(self, part):
        return self.all_realm_titles('',part)

    def get_descendents(self, building):
        descendents = []
        ancestors = [building]
        while len(ancestors) > 0:
            current = ancestors.pop()
            for abuilding in self.by_name.values():
                if abuilding.upgrades_from() == current:
                    ancestors.append(abuilding)
                    descendents.append(abuilding)
        return descendents

    def _handle_leftovers(self, ret, handled, context, block_count=0):
        to_be_handled = context.difference(handled)
        self._leftovers_handled = len(to_be_handled)
        single = False
        if len(to_be_handled) + block_count == 1:
            single = True
        for building in to_be_handled:
            # if building not in handled and building in context:
            ret += self.gen_building(building, single)
            handled.add(building)
        return ret, handled

    def _make_tb_weird(self, ret, atype):
        if atype in defines.EXTRA_HOLDING_TYPES:
            ret = set_event_target(defines.ABET_LOCATION) + ret
            # need to set province flags for
            #   extra holding types.
            ret = flag_prov_tech_levels() + ret
        else:
            ret = set_event_target(defines.ABET_HOLDING) + ret

        so=set_event_target(defines.ABET_OWNER)
        if atype == 'trade_post':
            ret = make_b('trade_post_owner',
                         set_event_target(defines.ABET_OWNER))+ret                         
            return make_b('any_trade_post', 
                make_if(has_no_camp(atype),ret))
        if atype == 'hospital':
            so=make_b('county',make_b('owner',so))
            return make_b('any_hospital', 
                make_if(has_no_camp(atype),so+ret))
        if atype == 'fort':
            so=make_b('county',make_b('owner',so))
            return make_b('any_fort', 
                make_if(has_no_camp(atype),so+ret))
        if atype == 'family_palace':            
            return so+make_b('family_palace', 
                make_if(has_no_camp(atype),ret))
        return self._make_title_selection_block(ret)

    def gen_type_weird(self, handled, context, atype):
        # so downstream classes can make decisions based off it.
        self._cur_type = atype
        handled = handled or set()
        context = context or set()
        ret = ''
        if atype not in self.by_type:
            return ret, handled

        modified_context = self.by_type[atype].intersection(context)
        # buildings with preqs and potential
        if modified_context:
            preq, handled = self.gen_prerequisites(handled, modified_context)
            # buildings with just preqs
            potential, handled = self.gen_potential(handled, modified_context)
            # buildings with just preqs
            tech, handled = self.gen_tech(handled, modified_context)
            type_part = preq + potential + tech
            # buildings with just types
            inner_blocks = (
                self._gen_prerequisites_blocks + self._gen_potential_blocks)
            type_part, handled = self._handle_leftovers(
                type_part, handled, modified_context, inner_blocks)
            inner_blocks += self._leftovers_handled
            if type_part != '':
                ret = type_part
        ret = self._add_note(ret, 'because {} are weird...'.format(atype))
        return ret, handled

    def gen_type_hospital(self, handled=None, context=None):
        return self.gen_type_weird(handled, context, 'hospital')

    def gen_type_fort(self, handled=None, context=None):
        return self.gen_type_weird(handled, context, 'fort')

    def gen_type_trade_post(self, handled=None, context=None):
        return self.gen_type_weird(handled, context, 'trade_post')

    def gen_type_fp(self, handled=None, context=None):
        return self.gen_type_weird(handled, context, 'family_palace')

    def gen_type(self, handled=None, context=None, types_to_build=None):
        handled = handled or set()
        context = context or set()
        self._gen_type_blocks = 0
        ret = ''
        verboten = ['family_palace', 'trade_post', 'hospital']
        for atype in self.by_type:
            if atype not in verboten:
                # so downstream classes can make decisions based off it.
                self._cur_type = atype
                modified_context = self.by_type[atype].intersection(context)
                if modified_context:
                    # builidngs with either a preq, or preq and potential or
                    # tech
                    preq, handled = self.gen_prerequisites(handled,
                                                           modified_context)
                    # buildings with just a potential, or potential + tech
                    potential, handled = self.gen_potential(handled,
                                                            modified_context)
                    # buildings with just a tech
                    tech, handled = self.gen_tech(handled, modified_context)

                    type_part = preq + potential + tech
                    inner_blocks = (self._gen_prerequisites_blocks +
                                    self._gen_potential_blocks)
                    # here we add any 'unhandled' guys from this holding type.
                    type_part, handled = self._handle_leftovers(
                        type_part, handled, modified_context, inner_blocks)
                    inner_blocks += self._leftovers_handled
                    if type_part != '':
                        self._gen_type_blocks += 1
                        if not self._callback and inner_blocks > 1:
                            type_part = make_b('OR', type_part)
                        limit = assign('holding_type', atype)
                        ret = self._make_block(ret, limit, type_part)
        return ret, handled

    def gen_building(self, building, single=False):
        hba = get_has_building_variable_from_type(self.by_name[building].type(
        ))
        # user 'create' stuff goes here.
        need_and = False
        ret = ''
        if self._callback:
            note = 'select: {}'.format(building)
            ret += self._callback(self.by_name[building])
        else:
            note = '{} is available'.format(building)
        ret = self._add_note(ret, note)
        limit = ''
        if self.by_name[building].upgrades_from():
            need_and = True
            limit += self.by_name[building].conditions.upgrades_from_block()
        # if self.by_name[building].tech_required():
        #need_and = True
        #limit += self.by_name[building].conditions.tech_requirements_andblock()
        if self.by_name[building].exclusive_with():
            need_and = True
            limit += self.by_name[building].conditions.exclusve_with_andblock()
        reno = self.by_name[building].conditions.renovating()
        has_building = assign(hba, building)
        # if self.by_name[building].type() == 'trade_post':
        # has_building=make_b('location',has_building)
        limit += make_b('NOT', has_building + reno)
        if not self._callback and (not need_and or single):
            # in trigger scope and there's only one block...
            ret = limit + ret
        else:
            ret = self._make_block('', limit, ret)
            # this is what we do for potential...
        # print 'handling ',building, ret
        return ret

    def gen_prerequisites(self, handled=None, context=None):
        handled = handled or set()
        context = context or set()
        self._gen_prerequisites_blocks = 0
        ret = ''
        for preq in self.by_prerequisites:
            preqs = ''
            modified_context = self.by_prerequisites[preq].intersection(
                context)
            if modified_context:
                pot, handled = self.gen_potential(handled, modified_context)
                tech, handled = self.gen_tech(handled, modified_context)
                preqs, handled = self._handle_leftovers(
                    preqs, handled, modified_context,
                    self._gen_potential_blocks)
                inner_blocks = self._leftovers_handled + \
                    self._gen_potential_blocks
                preqs = preqs + pot + tech
                if preqs != '':
                    self._gen_prerequisites_blocks += 1
                    # for building in self.by_prerequisites[preq]:
                    # break
                    # limit =
                    # self.by_name[building].conditions.prerequisites_andblock()
                    limit = assign(
                        defines.SEM_BLD_PREQ_MASK.format(self.prerequisites_ids[preq]),
                        'yes')
                    note = 'prequisite from ' + ','.join(modified_context)
                    limit = self._add_note(limit, note)
                    if not self._callback and inner_blocks > 1:
                        preqs = make_b('OR', preqs)
                    ret = self._make_block(ret, limit, preqs)
        return ret, handled

    def gen_potential(self, handled=None, context=None):
        handled = handled or set()
        context = context or set()
        self._gen_potential_blocks = 0
        ret = ''
        for potential in self.by_potential:
            part = ''
            mcontext = self.by_potential[potential].intersection(context)
            if mcontext:
                part, handled = self.gen_tech(handled, mcontext)
                part, handled = self._handle_leftovers(part, handled, mcontext)
                if part != '':
                    self._gen_potential_blocks += 1
                    limit = assign(
                        defines.SEM_BLD_POT_MASK.format(self.potenital_ids[potential]),
                        'yes')
                    note = 'modified potential from ' + ','.join(mcontext)
                    limit = self._add_note(limit, note)
                    if not self._callback and self._leftovers_handled > 1:
                        part = make_b('OR', part)
                    ret = self._make_block(ret, limit, part)
        return ret, handled

    def gen_tech(self, handled=None, context=None):
        handled = handled or set()
        context = context or set()
        self._gen_tech_blocks = 0
        ret = ''
        for tech in self.by_tech:
            part = ''
            mcontext = self.by_tech[tech].intersection(context)
            if mcontext:
                part, handled = self._handle_leftovers(part, handled, mcontext)
                if part != '':
                    self._gen_tech_blocks += 1
                    limit = assign(
                        defines.SEM_BLD_TECH_MASK.format(self.tech_ids[tech]), 'yes')
                    if self._cur_type not in defines.EXTRA_HOLDING_TYPES:
                        limit = make_b('location', limit)
                    note = 'modified tech from ' + ','.join(mcontext)
                    limit = self._add_note(limit, note)
                    if not self._callback and self._leftovers_handled > 1:
                        part = make_b('OR', part)
                    ret = self._make_block(ret, limit, part)
        return ret, handled
