import os
import sys

# this is because I'm using git submodules
sys.path.insert(1, os.path.join(os.getcwd(), 'lib', 'ck2lib'))
