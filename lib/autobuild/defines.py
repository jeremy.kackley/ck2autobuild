# this file holds defines that are relevant to the operation of AutoBuild.
# some of these defines might ultimately move into other places, since as:
#   A config file
#   a generic universal defines file.

from ck2.code.base import ScriptedEffects, ScriptedTriggers
from ck2.defines import EXTRA_HOLDING_TYPES

DEBUG_CONTROLFLOW = False  # generates lots of log messages
DEBUG_DISCOUNTS = False  # Logs things considered when defining discount
DEBUG_START_BUILDING = False #Logs details about what buildings are started.
DEBUG_SELECT_NOTES = False
DEBUG_SETS_REPORT = False  # generates a small text file.
COMBINE_TRIGGERS_WITH_POTENTIAL = True

HOLDING_TYPE_PH_PREFIXES = {
    'castle': 'ca_',
    'temple': 'tp_',
    'family_palace': 'fp_',
    'city': 'ct_',
    'trade_post': 'tr_',
    'tribal': 'tb_',
    'nomad': 'no_',
    'hospital': 'ho_',
    'fort': 'fo_',
}

# decision menu flag related defines
AUTOBUILD_MENU_FLAG = 'ab_config_menu_open'
AUTOBUILD_ENABLED_FLAG = 'ab_is_enabled_for_this_character'
RESERVE_FLAG_MASK = "ab_{}_reserve_{}_flag"
GOLD_RESERVE_POINTS = {}
PRESTIGE_RESERVE_POINTS = {}
for temp in [100, 250, 500, 1000, 2500, 5000, 10000]:
    GOLD_RESERVE_POINTS[RESERVE_FLAG_MASK.format('gold', temp)] = temp
    PRESTIGE_RESERVE_POINTS[RESERVE_FLAG_MASK.format('prestige', temp)] = temp
del temp
FILTER_FLAG_MASK = "ab_enable_{}_flag"
VASSALBARON_FLAG = FILTER_FLAG_MASK.format('vassal_baron')
COUNTYBARON_FLAG = FILTER_FLAG_MASK.format('county_baron')
ALLVASSAL_FLAG = FILTER_FLAG_MASK.format('all_vassal')
ALL_SETTINGS_FLAGS = [
    VASSALBARON_FLAG,
    COUNTYBARON_FLAG,
    ALLVASSAL_FLAG,
] + GOLD_RESERVE_POINTS.keys() + PRESTIGE_RESERVE_POINTS.keys()
PROVINCE_TECH_FLAG = "ab_tech_con_level_{}"

# EVENT TARGET DEFINES
ABET_PAYEE = 'abet_payee'
ABET_OWNER = 'abet_owner'
ABET_LOCATION = 'abet_location'
ABET_HOLDING = 'abet_holding'

WORKERS_CAMP_NAMEPART = "workers_camp"
# Building related masks
BUILDING_PH_MASK = '{}ab_ph_{}'
MODIFIER_MASK = '{}_ab_mod_{}'
DECISION_MASK = '{}_ab_dec_{}'

# namespace masks
START_NS_MASK = '{}_ab_start'
FINISH_NS_MASK = '{}_ab_finish'
SURVEY_NS_MASK = 'u_ab_survey_{}'  # one per set?
INSPECT_NS_MASK = '{}_ab_inspect_{}'  # one per building type
LORD_NS_MASK = '{}_ab_start_{}'
BUDGET_NS_MASK = '{}_ab_start_{}'
PAYER_NS_MASK = '{}_ab_start_{}'

# CONTROL FLOW DEFINES
ABCF_INIT = 'ab_cf_init_event'
ABCF_START = 'ab_cf_start_event'
ABCF_REPEAT = 'ab_cf_repeat_event'
ABCF_REACT = 'ab_cf_react_event'
ABCF_ON_DEATH = 'ab_cf_on_death_event'
ABCF_RESTART = 'ab_cf_restart_event'
ABCF_YEARLY_PULSE = 'ab_cf_yearly_pulse_event'  # e.g. pulse

ABCF_REACT_FLAG = 'ab_cf_reacting'
ABCF_REPEAT_FLAG = 'ab_cf_repeat_enqueued'
ABCF_BUILT_FLAG = 'ab_cf_built_something'
ABCF_RCS_FLAG = 'ab_cf_repeat_called_start'
ABCF_SELECTING_FLAG = 'ab_cf_selecting_something'
ALL_CONTROL_FLAGS = [
    ABCF_REACT_FLAG, ABCF_REPEAT_FLAG, ABCF_BUILT_FLAG, ABCF_RCS_FLAG,
    ABCF_SELECTING_FLAG
]

ABCF_BRANCHES = {}
for temp in HOLDING_TYPE_PH_PREFIXES:
    ABCF_BRANCHES[temp] = 'ab_cf_selecting_{}'.format(temp)
del temp

GRAPH_ABOUT_STR = """
These are graph files of building trees, in the Graph Modeling Language (GML) format.

They were created to aid debugging, but were included for kicks.

They can be opened with several tools, such as the yED graph editor, though
note that they are not pre-arranged, and will have to be arranged after opening.
Editors such as yED can do this automatically.
"""

# scripted effects masks
SEM_GRB_MASK = 'ab_available_gold_at_least_{:04d}'
SEM_PRB_MASK = 'ab_available_prestige_at_least_{:04d}'
SEM_BLD_PREQ_MASK = 'ab_preq_{}'
SEM_BLD_POT_MASK = 'ab_pot_{}'
SEM_BLD_TECH_MASK = 'ab_tech_{}'
SEM_BLD_UGF_MASK = 'ab_upgrade_{}'
SEM_BLD_EXW_MASK = 'ab_exclusive_{}'

SE_FLAG_PROVINCE = 'ab_flag_province_tech'
SE_CLR_PROVINCE_FLAGS = 'ab_clr_province_tech_flags'

GENERATE_GRAPHS_GML=True
GENERATE_GRAPHS_TGF=False
GENERATE_GRAPHS_DOT=False

#these are per-mod custom blocks that 
#   enhance menu compatibility.
EXTRA_CHECK_BLOCKS = {
    'Birthright2': """
                    #extra check from birthright2
                    NOT = {
                        has_character_flag = employment_decisions_open
                        has_character_flag = prisoner_decisions_open
                        has_character_flag = bloodline_decisions_open
                        has_character_flag = birthright_decisions_open
                        has_character_flag = festivity_decisions_open
                        has_character_flag = knighthood_decisions_open
                    }""",
}
