import re

from Building_Commands import Building_Commands
from Building_Conditions import Building_Conditions
from ck2.code.base import assign, make_b, make_event_target
from ck2.constructs.Building import Building
from common.functions import get_camp_name, get_placeholder_name, get_modifier_name
from defines import (ABET_HOLDING, ABET_LOCATION, ABET_OWNER,
                     COMBINE_TRIGGERS_WITH_POTENTIAL)


class BuildingAB(Building):
    '''
        Extends generic building adding autobuild
        specific methods and variables.
    '''

    def __init__(self, holding_type, building_parse, manager=None):
        Building.__init__(self, holding_type, building_parse)
        self.manager = manager
        # these are set by other parties, so it's
        #   actually an error if they never are set, ergo
        #   I'd prefer a compile-error rather than an obscure
        #   logic error.  Leaving them here so that
        #   programmers know they can expect them to exist.
        # self.finish_event=None
        # self.start_event=None
        # self.pay_event=None
        # self.refund_event=None
        self.conditions = Building_Conditions(self)
        self.commands = Building_Commands(self)

    def get_camp(self):
        return get_camp_name(self.type())

    def get_placeholder(self):
        return get_placeholder_name(self.name(), self.type())

    def modifier(self):
        return get_modifier_name(self.name(), self.type())

    def decision(self):
        return get_decision_name(self.name(), self.type())

    def get_trigger_non_tech(self):
        tb=None
        if self._building_parse.get_block('trigger'):
            tb_block= self._building_parse.get_block('trigger')
            tb=str(tb_block)
            tb = tb.partition('{')[2]
            tb = tb.rpartition('}')[0]            
            tb = re.sub('#.*\n','',tb)#get rid of comments
            tb = tb.replace('  ',' ')#get rid fo double spaces
            if self.tech_required():                
                for thing in self.tech_required():
                    tech=str(thing).partition('=')[0].strip()
                    level=str(thing).partition('=')[2].strip()
                    tb=tb.replace(str(thing).strip(),'')
                    if tech in tb:
                        print repr(tb), repr(tech)                    
                tb = tb.replace('  ',' ')#get rid fo double spaces
                tb=tb.replace('\n \n','\n')
                tb=tb.replace('FROM = { }','')
                tb = tb.strip()#get rid of trailing / leading return characters
            for custom_tooltip in tb_block.get_all_blocks('custom_tooltip'):         
                tb=tb.replace(str(custom_tooltip).strip(),'')
            if tb and tb.partition('=')[0].strip()!='location':
                tb=self.fix_scopes_and_wrap(tb)
        return tb

    def fix_scopes_and_wrap(self,limit):
        if not self.is_extra_holding_type():
            limit = limit.replace('FROMFROM',
                                    'event_target:{}'.format(ABET_HOLDING))
            # extra holding types start in the province block.
            limit = make_b('location', limit)
        else:
            limit = limit.replace('FROMFROM',
                                    'event_target:{}'.format(ABET_LOCATION))
        limit = limit.replace('FROM', 'event_target:{}'.format(ABET_OWNER))
        return limit

    def potential(self):
        limit = Building.potential(self)
        if limit:
            limit = limit.pretty()
            # now fix it up to work with autobuild
            limit = limit.partition('{')[2]
            limit = limit.rpartition('}')[0]
        else:
            limit = ''
        if self.requires_port():
            limit = limit+' '+assign('port', 'yes')
        if limit:
            limit = self.fix_scopes_and_wrap(limit)
        if COMBINE_TRIGGERS_WITH_POTENTIAL:
            tb = self.get_trigger_non_tech()
            if tb:
                limit = limit + ' ' +tb
        return limit


def BuildingABfromBuilding(building, manager=None):
    new = BuildingAB(building._holding_type, building._building_parse, manager)    
    new.localized_names = building.localized_names
    new._technology = building._technology
    return new
