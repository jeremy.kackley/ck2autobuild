from ck2.code.base import assign, make_b, trigger         
from .. import defines                     

#this might not should be global, but pending a refactor, it's fine for now.
def get_has_building_variable_from_type(building_type):
    if building_type == 'trade_post':
        return 'trade_post_has_building'
    if building_type == 'fort':
        return 'fort_has_building'
    if building_type == 'hospital':
        return 'hospital_has_building'
    return 'has_building'

    #any_demesne_province
    #any_province_holding

#maybe common ins't quite the proper place for this,
#   but it was that or callbacks.

def repeat_event(aid, days='', tooltip='', random=''):
    return trigger('character_event', aid, days, tooltip, random)


def adjust_number_by_tech_level(number, level, max_discount=-0.25):
    per_step = max_discount / 8.0
    number = int(float(number))
    adjusted = int(number + number * (per_step * level))
    if adjusted < 1:
        adjusted = 1
    return adjusted


#@make_scripted_object('ateh')
def add_to_extra_holding(atype, building):
    atype = assign('type', atype)
    building = assign('building', building)
    return make_b('add_to_extra_holding', atype + building)


#@make_scripted_object('dieh')
def destroy_in_extra_holding(atype, building):
    atype = assign('type', atype)
    building = assign('building', building)
    return make_b('destroy_in_extra_holding', atype + building)


def add_comment(ret, note, maxlength=100):
    if ret == '':
        return ret
    if len(note) > maxlength:
        note = note[:50] + '...'
    ret = '#{}\n'.format(note) + ret
    return ret


def any_demesne_holding(content, set_et=True):
    only_valid = make_b('limit', holding_is_valid_autobuild_target())
    ret = make_b('any_province_holding', only_valid + content)
    #temporary home for this.
    ret = flag_prov_tech_levels() + ret
    if set_et:
        ret = set_event_target(ABET_LOCATION) + ret
    return make_b('any_demesne_province', ret)


def get_type_ph_prefix(atype):
    return defines.HOLDING_TYPE_PH_PREFIXES[atype]


def get_placeholder_name(name, atype):
    return defines.BUILDING_PH_MASK.format(get_type_ph_prefix(atype), name)

def get_camp_name(atype):
    return get_placeholder_name(defines.WORKERS_CAMP_NAMEPART, atype)

def get_modifier_name(name, atype):
    return defines.MODIFIER_MASK.format(get_type_ph_prefix(atype), name)


def get_decision_name(name, atype):
    return defines.DECISION_MASK.format(get_type_ph_prefix(atype), name)


def sanitize_potential_string(string):
    string = key.replace('FROMFROM', 'event_target:' + ABET_HOLDING)
    string = key.replace('FROM', 'event_target:' + ABET_OWNER)
    return string
