import ck2.code.base
from ck2.code.base import assign, log, make_b, make_scripted_object
from ck2.code.cmd import clr_character_flag
from ck2.code.cond import has_character_flag
from ck2.code.scope import liege, owner, primary_title
from functions import add_comment

from ..defines import (ABCF_BUILT_FLAG, ALL_CONTROL_FLAGS, ALL_SETTINGS_FLAGS,
                       DEBUG_CONTROLFLOW, SE_CLR_PROVINCE_FLAGS,
                       SE_FLAG_PROVINCE)


def clr_province_tech_level_flags():
    return assign(SE_CLR_PROVINCE_FLAGS,'yes')

def flag_prov_tech_levels():
    #the code to do this has migrated into the test, this function
    #   is a temporary measure as I figure out where the function
    #   should live.
    return assign(SE_FLAG_PROVINCE,'yes')

@make_scripted_object('holding_is_valid_autobuild_target')
def holding_is_valid_autobuild_target():
    is_baron = owner('', primary_title('', assign('tier', 'baron')))
    is_ba_target = add_comment(
        make_b('ROOT', has_character_flag(ALLVASSAL_FLAG)),
        'all vassals target')

    is_vassal = owner('', liege('', assign('character', 'ROOT')))
    build_vb = make_b('ROOT', has_character_flag(VASSALBARON_FLAG))
    is_bv_target = add_comment(
        make_b('AND', is_vassal + build_vb), 'vassal baron target')
    # if Barons in Counties:
    #   Baron Vassals in My held Counties.
    #   any_realm_title owner.liege=ROOT, tier=baron, location.owner=ROOT
    #   tier=baron
    #   or = {}
    is_my_county = make_b('county', owner('', assign('character', 'ROOT')))
    build_cb = make_b('ROOT', has_character_flag(COUNTYBARON_FLAG))
    is_cb_target = add_comment(
        make_b("AND", is_my_county + build_cb), 'county baron target')

    is_baron_target = make_b('AND', make_b('OR', is_bv_target + is_cb_target))
    is_target = make_b('OR', is_baron_target + is_ba_target)
    return is_target


@make_scripted_object('autobuild_select_building')
def autobuild_select_building():
    set_flag = make_b('ROOT', assign('set_character_flag', ABCF_BUILT_FLAG))
    return set_flag


@make_scripted_object('ab_set')
def set_event_target(event_target_string):
    ret = ck2.code.base.set_event_target(event_target_string)
    lm = ''
    if DEBUG_CONTROLFLOW:
        lm = log('{}'.format(event_target_string),
                 'set to [{}.GetID]'.format(event_target_string))
    ret = ret + lm
    return ret

@make_scripted_object('clr_all_autobuilding_flags')
def clr_all_autobuilding_flags():
    flags = ''
    for flag in ALL_CONTROL_FLAGS + ALL_SETTINGS_FLAGS:
        flags += clr_character_flag(flag)
    return flags 

#other sripted effects considered at one time included, but not currently implemented included:
    #set_holding,
    #set_location,
    #set_owner,
    #set_payee,
    #destroy_in_extra_holding,
    #add_to_extra_holding,
SCRIPTED_EFFECTS = [
    holding_is_valid_autobuild_target,
    autobuild_select_building,
    set_event_target,
    clr_all_autobuilding_flags
]
