from ck2.code.base import assign, make_b, make_scripted_object
from ck2.code.cond import has_character_flag
from ck2.code.scope import county, liege, owner, primary_title
from ..defines import (ABCF_BUILT_FLAG, ALLVASSAL_FLAG, COUNTYBARON_FLAG,
                     EXTRA_HOLDING_TYPES, HOLDING_TYPE_PH_PREFIXES, VASSALBARON_FLAG)
from functions import get_camp_name,add_comment

@make_scripted_object('ab_renovating_any')
def renovating_any():
    ret = ''
    for atype in HOLDING_TYPE_PH_PREFIXES:
        if atype not in EXTRA_HOLDING_TYPES:
            #extra holding types don't disqualify a title from building there.
            if atype == 'trade_post':
                hba = 'trade_post_has_building'
            if atype == 'hospital':
                hba = 'hospital_has_building'
            hba = 'has_building'
            temp = assign(hba, get_camp_name(atype))
            ret += temp
    return make_b('OR', ret)


@make_scripted_object('ab_not_renovating_any')
def not_renovating_any():
    #hard coded for now.
    return make_b('NOT', renovating_any.get_result())


@make_scripted_object('holding_is_valid_autobuild_target')
def holding_is_valid_autobuild_target():
    is_baron = owner('', primary_title('', assign('tier', 'baron')))
    is_ba_target = add_comment(
        make_b('ROOT', has_character_flag(ALLVASSAL_FLAG)),
        'all vassals target')

    is_vassal = owner('', liege('', assign('character', 'ROOT')))
    build_vb = make_b('ROOT', has_character_flag(VASSALBARON_FLAG))
    is_bv_target = add_comment(
        make_b('AND', is_vassal + build_vb), 'vassal baron target')
    # if Barons in Counties:
    #   Baron Vassals in My held Counties.
    #   any_realm_title owner.liege=ROOT, tier=baron, location.owner=ROOT
    #   tier=baron
    #   or = {}
    is_my_county = make_b('county', owner('', assign('character', 'ROOT')))
    build_cb = make_b('ROOT', has_character_flag(COUNTYBARON_FLAG))
    is_cb_target = add_comment(
        make_b("AND", is_my_county + build_cb), 'county baron target')

    is_my_title = add_comment(
        owner('', assign('character', 'ROOT')), 'is my title')

    is_baron_target = make_b(
        'AND', is_baron + make_b('OR', is_bv_target + is_cb_target))
    is_target = make_b('OR', is_baron_target + is_ba_target + is_my_title)
    return is_target


@make_scripted_object('extra_holding_is_valid_autobuild_target')
def extra_holding_is_valid_autobuild_target():
    is_my_title = owner('', assign('character', 'ROOT'))
    is_my_county = county('', is_my_title)
    return make_b('OR', is_my_county + has_character_flag(ALLVASSAL_FLAG))


@make_scripted_object('autobuild_not_selected_building')
def autobuild_not_selected_building():
    return make_b('NOT', make_b('ROOT', has_character_flag(ABCF_BUILT_FLAG)))

@make_scripted_object('ab_has_camp')
def has_camp(atype):
    if atype=='hospital':
        return assign('hospital_has_building',get_camp_name(atype))
    if atype=='trade_post':
        return assign('trade_post_has_building',get_camp_name(atype))
    return assign('has_building',get_camp_name(atype))

@make_scripted_object('ab_has_no_camp')
def has_no_camp(atype):
    return make_b('NOT',has_camp.get_result(atype))

SCRIPTED_TRIGGERS = [
    renovating_any,
    not_renovating_any,
    holding_is_valid_autobuild_target,
    extra_holding_is_valid_autobuild_target,
    autobuild_not_selected_building,
    has_camp,
    has_no_camp,
]
