from ck2.code.base import assign, make_b, province_event
from ck2.code.cmd import add_province_modifier, province_event
from ck2.code.cmd import add_province_modifier
from common.functions import destroy_in_extra_holding, add_to_extra_holding

class Building_Commands:
    def __init__(self, building):
        self.bldg = building

    def call_finish_event(self, days=None):
        # Construction - Reduces construction time and cost
        #    by 3.1%/level (25% at level 8)
        #    this is very bloaty...since the only easy way is to
        #    generate 8 potential start_events per thing....
        #        this isn't a huge deal since it's buried
        #        in start_event, but still...
        if not days:
            days = self.bldg.build_time()
        return province_event(self.bldg.finish_event, days)

    def call_start_event(self):
        return province_event(self.bldg.start_event)

    def call_pay_event(self):        
        return province_event(self.bldg.pay_event)

    def call_refund_event(self):
        return province_event(self.bldg.refund_event)

    def add_province_modifier(self, days=None):
        if days == None:
            days = self.bldg.build_time()
        return add_province_modifier(self.bldg.modifier(), days)

    def start_renovating(self):
        ret = ''
        if self.bldg.is_extra_holding_type():
            ret += add_to_extra_holding(self.bldg.type(), self.bldg.get_camp())
            ret += add_to_extra_holding(self.bldg.type(),
                                        self.bldg.get_placeholder())
        else:
            ret = assign('add_building', self.bldg.get_camp())
            ret += assign('add_building', self.bldg.get_placeholder())
        return ret

    def stop_renovating(self):
        ret = ''
        if self.bldg.is_extra_holding_type():
            ret += destroy_in_extra_holding(self.bldg.type(),
                                            self.bldg.get_camp())
            ret += destroy_in_extra_holding(self.bldg.type(),
                                            self.bldg.get_placeholder())
        else:
            ret = assign('remove_building', self.bldg.get_camp())
            ret += assign('remove_building', self.bldg.get_placeholder())
            ret += assign('remove_province_modifier', self.bldg.modifier())
        return ret

    def pay_prestige(self):
        prc = self.bldg.prestige_cost()
        if prc:
            return 'prestige = -{}'.format(prc)
        return ''
