import defines
from common.functions import get_has_building_variable_from_type,get_camp_name
from ck2.code.base import assign, make_b


class Building_Conditions:
    '''
        This class collects code relating to creating building-related
            ck2 mod logic.

        Intended to extend the 'generic' building...
            building.condition.has_placeholder()
            building.command.start_reno()
            building.command.trigger_finish()
            building.placeholder
    '''

    def __init__(self, building):
        self.bldg = building

    def upgrades_from_block(self):
        hba = get_has_building_variable_from_type(self.bldg.type())
        return assign(hba, self.bldg.upgrades_from())

    def tech_requirements_andblock(self):
        tech_required = '\n'.join(self.bldg.tech_required())
        if not self.bldg.is_extra_holding_type():
            # extra holding types scope to provinces, not titles.
            tech_required = make_b('location', tech_required)
        return tech_required

    def has_placeholder(self):
        hba = get_has_building_variable_from_type(self.bldg.type())
        return assign(hba, self.bldg.get_placeholder())

    def renovating_any(self):
        return renovating_any()

    def renovating(self):
        hba = get_has_building_variable_from_type(self.bldg.type())
        # checking for the camp after checking for the presence of any camp
        #   is redundant, UNLESS you are an extra holding type
        #ret = assign(hba, get_camp_name(self.bldg.type()))
        ret = ''
        if self.bldg.is_extra_holding_type():
            ret += assign(hba, get_camp_name(self.bldg.type()))
        ret += self.has_placeholder()
        return ret

    def not_renovating(self):
        content = self.renovating()
        return make_b('NOT', content)

    def exclusve_with_andblock(self):
        ret = ''
        if self.bldg.exclusive_with():
            hba = get_has_building_variable_from_type(self.bldg.type())
            tmp = [assign(hba, x) for x in self.bldg.prerequisites()]
            ret = make_b('NOT', '\n'.join(tmp))
        return ret

    def prerequisites_andblock(self):
        ret = ''
        if self.bldg.prerequisites():
            hba = get_has_building_variable_from_type(self.bldg.type())
            tmp = [assign(hba, x) for x in self.bldg.prerequisites()]
            if len(self.bldg.prerequisites()) > 1:
                ret += make_b('AND', '\n'.join(tmp))
            else:
                ret += ''.join(tmp)
        return ret
