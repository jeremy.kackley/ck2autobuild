# -*- coding: utf-8 -*-
from ck2.modbase import Localization
from ck2.code.base import make_b
from localization_strings import AB_LS


class ModifierGenerator:
    '''
    Creates/manages province modifiers, basically.
    '''

    def __init__(self, parent):
        self.parent = parent
        self._pretty_name = "Modifiers"
        self._loca = {}

    def create(self, building):
        modname = building.modifier()
        self._loca[modname] = building
        ret = make_b(modname, 'icon = 7')
        return ret

    def gen_loca(self):
        ret = []
        for name in sorted(self._loca):
            building = self._loca[name]
            new = Localization(name)
            new.filename = 'modifiers_{}.csv'.format(building.type())
            for mask, lang in new.walk(AB_LS['MODIFIER_MASK']):
                string = mask.format(
                    building.type_l(lang),
                    building.name_l(lang), building.build_time())
                new.add(string, lang)
            ret.append(new)
        return ret
