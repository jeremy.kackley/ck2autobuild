import string
from operator import attrgetter

from BuildingAB import BuildingABfromBuilding
from BuildingSetsAB import BuildingSetsAB
from ck2.code.base import Building, assign, make_b
from ck2.constructs.Building import TYPE_LOCALIZATION
from ck2.modbase import Localization
from common.functions import get_camp_name, get_has_building_variable_from_type, get_placeholder_name
from defines import GOLD_RESERVE_POINTS, PRESTIGE_RESERVE_POINTS
from localization_strings import *


def make_prestige_block(pcost):
    if not pcost:
        return ''
    pcost = int(pcost)
    ret = ''
    flags = ''
    for flag, amnt in PRESTIGE_RESERVE_POINTS.items():
        flag = assign('has_character_flag', flag)
        flags += flag
        wealth = assign('prestige', pcost + amnt)
        ret += make_b('AND', flag + wealth)
    ret += make_b('AND', make_b('NOT', flags) + assign('prestige', pcost))
    return make_b('OR', ret)


def make_gold_block(acost):
    # if I have no 'wealth' based cost, then I don't
    #   need this block, though I probably do need
    #   prestige reserves.
    if not acost:
        return ''
    acost = int(float(acost))
    ret = ''
    flags = ''
    for flag, amnt in GOLD_RESERVE_POINTS.items():
        flag = assign('has_character_flag', flag)
        flags += flag
        wealth = assign('wealth', acost + amnt)
        ret += make_b('AND', flag + wealth)
    ret += make_b('AND', make_b('NOT', flags) + assign('wealth', acost))
    return make_b('OR', ret)


def make_st_comment(what, buildings):
    mask = '{} from {} building(s) {}'
    comment = mask.format(what, len(buildings), ','.join(buildings))
    if len(comment) > 70:
        comment = comment[:70] + '...'
    return ''


class Building_Manager:
    '''
        * Generates faux buildings.
        * provides convience functions for generating building related actions.
        * maintains links to start event/end events.
        * Maintains building sets.
        * Provides interface to raw sets data.
        * Generates Graphs: from buildings using Graph class
                * By Type
                * By Building

    '''

    def __init__(self, parent):
        self.parent = parent
        self.sets = BuildingSetsAB()
        self._prefixes = HOLDING_TYPE_PH_PREFIXES
        self.placeholder_mask = BUILDING_PH_MASK
        self._placeholder_names = {}
        self._camp_names = {}
        self._found_unique = False

    def has_unique_buildings(self):
        return self._found_unique

    def gen_scripted_triggers(self):
        for gold in self.sets.by_gold:
            if gold:
                self.parent.st.add(
                    SEM_GRB_MASK.format(gold),
                    make_gold_block(gold), 'gold_reserves')

        for prestige in self.sets.by_prestige:
            if prestige:
                buildings = self.sets.by_prestige[prestige]
                comment = make_st_comment("prestige", buildings)
                name = SEM_PRB_MASK.format(prestige)
                block = make_prestige_block(prestige)
                self.parent.st.add(name, block, 'prestige_reserves', comment)

        for potential, pid in self.sets.potenital_ids.items():
            buildings = self.sets.by_potential[potential]
            comment = make_st_comment("potential", buildings)
            name = SEM_BLD_POT_MASK.format(pid)
            self.parent.st.add(name, potential, 'potentials', comment)

        for preq, pid in self.sets.prerequisites_ids.items():
            buildings = self.sets.by_prerequisites[preq]
            comment = make_st_comment("prerequisites", buildings)
            block = self.sets.by_name[pid].conditions.prerequisites_andblock()
            name = SEM_BLD_PREQ_MASK.format(pid)
            self.parent.st.add(name, block, 'prerequisites', comment)

        for preq, pid in self.sets.tech_ids.items():
            block = '\n'.join(self.sets.by_name[pid].tech_required())
            buildings = self.sets.by_tech[preq]
            comment = make_st_comment("tech_required", buildings)
            name = SEM_BLD_TECH_MASK.format(pid)
            self.parent.st.add(name, block, 'tech_required', comment)

    def is_placeholder(self, building):
        is_camp = building.name() == get_camp_name(building.type())
        is_placeholder = building.name() in self._placeholder_names
        is_placeholder_of_type = get_placeholder_name('',building.type()) in building.name()
        if is_camp and is_placeholder and is_placeholder_of_type:
            return True
        return False

    def add(self, building):
        if not self.is_placeholder(building):
            self.sets.add(BuildingABfromBuilding(building, self))
            self._found_unique = True

    def get_buildings(self):
        return self.sets.get_all_buildings()

    def gen_phs(self):
        ret = []
        self._placeholder_names = {}
        # sort buildings before generating for somewhat cleaner files.
        for building in self.get_buildings():
            name = building.get_placeholder()
            self._placeholder_names[name] = building.name()
            tmp = self._create_faux(name, building.type())
            ret.append(tmp)
        return ret

    def _create_faux(self, name, atype):
        desc = name + '_desc'
        new = Building(name, desc, atype)
        hba = get_has_building_variable_from_type(atype)
        a_hba = assign(hba, name)
        if atype not in EXTRA_HOLDING_TYPES:
            # trade_posts (and hospitals) have to have their potential called
            # at the province level, but the rest call it from the holding
            a_hba = make_b('FROMFROM', a_hba)
        new.add_potential(a_hba)
        new.add_trigger(assign('always', 'no'))
        new.extra_tech_building_start = '10.0'
        return new

    def gen_loca(self):
        ret = []
        fnmask = 'ph_buildings_{}.csv'
        for atype in sorted(self._camp_names):
            new = Localization(self._camp_names[atype])
            new.filename = fnmask.format(atype)
            for mask, lang in new.walk(AB_LS['CAMP_NAME_MASK']):
                mask = mask.format(TYPE_LOCALIZATION[atype][lang])
                mask = string.capwords(mask)
                new.add(mask, lang)
            ret.append(new)
            new = Localization(self._camp_names[atype] + '_desc')
            new.filename = fnmask.format(atype)
            for mask, lang in new.walk(AB_LS['CAMP_DESC_MASK']):
                new.add(mask.format(TYPE_LOCALIZATION[atype][lang]), lang)
            ret.append(new)
        for building in self.get_buildings():
            new = Localization(building.get_placeholder())
            new.filename = fnmask.format(building.type())
            for mask, lang in new.walk(AB_LS['BUILDING_PH_NAME_MASK']):
                new.add(mask.format(building.name_l(lang)), lang)
            ret.append(new)
            new = Localization(building.get_placeholder() + '_desc')
            new.filename = fnmask.format(building.type())
            for mask, lang in new.walk(AB_LS['BUILDING_PH_DESC_MASK']):
                new.add(
                    mask.format(building.name_l(lang), building.build_time()),
                    lang)
            ret.append(new)
        return ret

    def gen_camps(self):
        ret = []
        self._camp_names = {}
        for value in sorted(self._prefixes):
            name = get_camp_name(value)
            ret.append(self._create_faux(name, value))
            self._camp_names[value] = name
        return ret
