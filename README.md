This code generates mini mods (for both vanilla CK2 and other CK2 mods) that 
automatically build buildings in either your directly held demesne (e.g. county 
seats), baron vassals in your counties (e.g. county barons) or vassal barons 
(any direct vassal of yours whose primary title is a barony.) It honors build 
times, build costs, potentials, prequesitits, and technology requirements. 

It does attempt to apply technology, minister, and modifier bonuses (or maluses)
to construction time and cost, though coverage is not 100% due to limitations.

Includes the ability to configure a gold and prestige reserve in game.  Settings
should be copied to your heir on death, at least in most cases, and things re
enabled.

More technically, it parses the vanilla + mod added buildings, and generates an 
event to select a building which you can afford and meet the requirements, in a 
appropriate province, then adds a 'placeholder' building. After the build time 
has expired, the placeholder is replaced with the actual building (via event). 

It doesn't currently block you (or the AI) from manually constructing the 
building via the normal interface; in the event the building already exists 
when the 'finish' event fires, it refunds the construction cost.

Once enabled, the event will continue to fire periodically. It also fires on 
building completion. It utilizes character flags and events; and thus should 
work for AI or in multiplayer, though the AI will never use the decision that 
enables the event chain.

# How to Use:
If you just want to run this, just go to the [Ck2 Forum](https://forum.paradoxplaza.com/forum/index.php?threads/autobuild-ish-experimentation.788981/)

Otherwise, this requires the CK2 Library repo to be in the appropraite subfolder to run. It
also currently has hard-coded locations for the CK2 directory in the main file.
It should find your CK2 mods folder automatically, at least on Windows.

