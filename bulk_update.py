""" CK2 Auto Update/Deploy...
	Create a folder structure
		/(CK2 Version)/Vanilla/[vanilla files here]
		/(CK2 Version)/(Mod)/(Version,maybe NA)/[mod files here]
	I just add files to the tree.
	Run a script at the root directory that runs the script on each version/mod combination.
	On success (i.e. finishes without an error) it copy's the mod to the release archive directory.
		/CK2ReleaseArchive/(CK2 Version)/(Mod/Vanilla)/(Version,maybe NA)/
	On update of a release, copy to an /OutBound directory.

	Can accomplish this by adding some basic basic command line parameters:
		-s = CK2 game directory.
		-m = Mod files directory.
		-d = Destination directory for output files.
		-z = zip contents.
	
	This would allow the calling script to specify where to find CK2 files, where to find the mods, and where to write to.
	The calling script could also compare the output with previously released files and copy things to an output dir.

    Current directory example: I:\My Random Stuff\CK2Dist\Data\2.8.3.X\Geheimnisnacht\1.2

    Though I might move this functionality to Jenkins.

    Consider changing the format; e.g.
        CK2Version/Mods/{all_mods}
    
    with version stuck in a readme.

    Also, make this expect/work with/create archives.  

    e.g. first pass:
        checks for unarchived mods, and converts them to archived versions.  (write a function to do that)
        generates any new mods as zip archives.

    add a hash function to ModInfo to see if it needs to generate a new mod?
    Alternatively, just add a third step to copy from the 'mod' directory to a 'release' directory when mods change.
    Generated mods can have a 'hash' and if I store the 'hashes' in the released directory I can only generate release zips
    when necessary.  
	 """

import argparse
import os
import subprocess

FORCE_REGEN = False #forces regeneration of all mods.  This should only be necessary when script has changed.

parser = argparse.ArgumentParser()
parser.add_argument('-s','--source', dest='source',default=r"D:\My Random Stuff\CK2Dist\Data",help='Bulk processing directory with mods and vanilla files, defaults to D:/My Random Stuff/CK2Dist/Data')
parser.add_argument('-d','--dest', dest='dest',default=r"D:\My Random Stuff\CK2Dist\Mods",help='Destination directory (defaults to D:/My Random Stuff/CK2Dist/Mods')
args = parser.parse_args()  
source=args.source
dest=args.dest
processes={}
names=[]
summary_template='Autobuild for CK2 {}, includes minimods for {} and {}.  Minimally tested, please report any issues.  Thanks.'
summary_template_nm='Autobuild for CK2 {}.  Minimally tested, please report any issues.  Thanks.'
for version in os.listdir(source):   
    summary_list=[] 
    name='Vanilla {}'.format(version)
    versionpath=os.path.join(source,version)
    vanillapath=os.path.join(versionpath,'Vanilla')
    moddest=os.path.join(dest,version,'Vanilla')
    print "Processing vanilla CK2 {}".format(version)
    command='python generate_auto_build.py -s "{}" -d "{}" --disable_multiprocessing --disable_mod_gen'.format(vanillapath,moddest)
    try:
        os.makedirs(moddest)
    except: 
        pass
    names.append(name)
    processes[name]=subprocess.Popen(command)
    print command
    for mod in os.listdir(versionpath):
        if 'Vanilla' != mod:
            modpath=os.path.join(versionpath,mod)
            for modversion in os.listdir(modpath):
                summary_list.append('{} ({})'.format(mod,modversion))
                name='{} ({}) for CK2 {}'.format(mod,modversion,version)
                print "Processing {}".format(name)
                modversionpath=os.path.join(modpath,modversion)
                moddest=os.path.join(dest,version,"{}_{}".format(mod,modversion))
                command='python generate_auto_build.py -s "{}" -d "{}" -m"{}" --disable_multiprocessing --disable_vanilla_gen'.format(vanillapath,moddest,modversionpath)
                if FORCE_REGEN or not os.path.isdir(moddest):
                    try:
                        os.makedirs(moddest)
                    except: 
                        pass
                    names.append(name)                
                    processes[name]=subprocess.Popen(command)
                    print command
    with open(os.path.join(dest,version,'description.txt'),'w') as _fp:
        if len(summary_list)>0:
            _fp.write(summary_template.format(version,', '.join(summary_list[:-1]),summary_list[-1]))
        else:
            _fp.write(summary_template_nm.format(version))
    
print 'Waiting for processes.'
for name in names:    
    result=None
    try:
        results=processes[name].wait()
    except:
        pass
    print "{} done.".format(name)