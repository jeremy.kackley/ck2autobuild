# -*- coding: utf-8 -*-
# pylint:disable=import-error
"""Summary

Attributes:
    BUILDING_EXCEPTION_LIST (TYPE): Description
    BUILDING_NAMEMASK (str): Description
    CREATE_IN_MOD_FOLDER (bool): Description
    CREATE_MINI_MODS (bool): Description
    DEBUGGING (bool): Description
    DECFILE_NAMEMASK (str): Description
    ENABLED_BUILDING_TYPES (TYPE): Description
    EVENT_NAMEMASK (str): Descriptions
    FILES_CREATED (TYPE): Description
    LOCA_NAMEMASK (str): Description
    MODIFIER_NAMEMASK (str): Description
    PROFILE (bool): Description
    USE_MULTIPROCESSING_AT_MOD_LEVEL (bool): Description
"""
import argparse
import cProfile
import os
import pstats
import re
import traceback
from datetime import datetime
from multiprocessing import Pool

from lib.autobuild.AutoBuilder import AutoBuilder2 as AutoBuilder
from lib.autobuild.defines import EXTRA_CHECK_BLOCKS

from ck2 import utilities as ck2

CREATE_IN_MOD_FOLDER = False
CREATE_MINI_MODS = True
DEBUGGING = False
DEBUG_EXTREME = False
USE_MULTIPROCESSING_AT_MOD_LEVEL = True
PROFILE = False
EXCLLUSION_CAST = '\texcluding {} due to its build time ({}) failing to cast to int.'
EXCLUSION_UNKOWN_TYPE = '\texcluding {} because {} is not a known building type.'
EXCLUSION_DISABLED = '\texcluding {} because {} buildings are disabled.'
EXCLUSION_BTLT1 = '\texcluding {} due to it having a build time of {}.'
EXCLUSION_NAME = "\texcluding building by name: {}."
CREATE_ZIP = False
CONVERT_MODS_TO_ZIP = False


def write_file(path, filename, data):
    """Summary

    Args:
        path (TYPE): Description
        filename (TYPE): Description
        data (TYPE): Description

    Returns:
        TYPE: Description
    """
    if data == '':
        return
    if not os.path.exists(path):
        os.makedirs(path)
    with open(path + filename, 'w') as _fp:
        _fp.write(data)

DECFILE_NAMEMASK = 'auto_build_decgen_{}.txt'
LOCA_NAMEMASK = 'auto_build_locagen_{}.csv'
EVENT_NAMEMASK = 'auto_build_evntgen_{}.txt'
MODIFIER_NAMEMASK = 'auto_build_modifgen_{}.txt'
BUILDING_NAMEMASK = 'auto_build_fauxbuilding_{}.txt'
FILES_CREATED = [
    'decisions\\' + DECFILE_NAMEMASK,
    'localisation\\' + LOCA_NAMEMASK,
    'events\\' + EVENT_NAMEMASK,
    'common\\event_modifiers\\' + MODIFIER_NAMEMASK,
]

# disabling building types causes a weird bug with selection.  Needs
# investigation.
ENABLED_BUILDING_TYPES = {
    'city': True,
    'castle': True,
    'temple': True,
    'tribal': True,
    'nomad': True,
    'trade_post': True,
    'family_palace': True,
    'hospital': True,
    'fort': True,
}
BUILDING_EXCEPTION_LIST = [
    # 'ca_asoiaf_move_camp',
]


def filter_buildings(buildings):
    """Summary

    Args:
        buildings (TYPE): Description

    Returns:
        TYPE: Description
    """
    buildings_found = []
    for building in buildings:
        if building.name() != 'under_renovation':
            # not sure this is valid anymore.
            # under_renovation is our fake placeholder
            buildings_found.append(building)
            # this was useful for debuggery, and might prove
            #   useful again.
            if False and building.name() == 'ca_azanulimbardum':
                print repr(building.get_raw_parse())
                print repr(building.get_raw_parse().get_block('potential'))
                print building.get_raw_parse().get_block('potential')
                print building.get_raw_parse().pretty()
                print building.prerequisites()
                print building.upgrades_from()
                print building.potential()
                raise NameError('DEBUG_EXTREME')
    return buildings_found


def elapsed(start):
    """Summary

    Args:
        start (TYPE): Description

    Returns:
        TYPE: Description
    """
    elapsed = datetime.now() - start
    return str(elapsed)  # .partition('.')[0]


def building_exclusion_reason(building):
    """Summary

    Args:
        building (TYPE): Description

    Returns:
        TYPE: Description
    """

    reason = None
    if building.type() not in ENABLED_BUILDING_TYPES:
        return EXCLUSION_UNKOWN_TYPE.format(building.name(), building.type())
    else:
        if not ENABLED_BUILDING_TYPES[building.type()]:
            return EXCLUSION_DISABLED.format(building.name(), building.type())
    try:
        if int(building.build_time()) < 1:
            return EXCLUSION_BTLT1.format(building.name(),
                                          building.build_time())
    except TypeError:
        return EXCLLUSION_CAST.format(building.name(), building.build_time())
    if building.name() in BUILDING_EXCEPTION_LIST:
        return EXCLUSION_NAME.format(building.name())
    return reason


def generate_mod(mod,dest=None,write=True):
    """Summary

    Args:
        mod (TYPE): Description

    Returns:
        TYPE: Description
    """
    if CONVERT_MODS_TO_ZIP:
        mod.convert_to_zip()

    auto = AutoBuilder()

    processing_results = ''
    last = datetime.now()

    if DEBUG_EXTREME:
        # useful for debugging, primarily.
        buildings_found = filter_buildings(mod.buildings())
    else:
        buildings_found = mod.buildings()

    if len(buildings_found) > 0:
        processing_results += 'Processing ' + \
            mod.name + '...' + '\n'

        # Some buildings (generally mod buildings) not only
        #    don't make sense to autobuild, but also cause issues
        #    with interpreting the potential block.
        for building in buildings_found:
            reason = building_exclusion_reason(building)
            if reason:
                processing_results += reason + '\n'
            else:
                auto.add_building(building)

        processing_results += 'Finding {} buildings took {}.'.format(
            len(auto.bm.sets.by_name), elapsed(last)) + '\n'
        last = datetime.now()

        if len(auto.bm.sets.by_name) > 1:

            for action in mod.job_actions():
                auto.add_job_action(action)

            for tech in mod.technology():
                auto.add_tech(tech)

            for modifier in mod.event_modifiers():
                auto.add_event_modifier(modifier)

            for artifact in mod.artifacts():
                auto.add_artifact(artifact)

            for bloodline in mod.bloodlines():
                auto.add_bloodline(bloodline)

            if EXTRA_CHECK_BLOCKS.has_key(mod.name):
                auto.config.extra_check_block=EXTRA_CHECK_BLOCKS[mod.name]

            auto.add_readme('script_output.txt', processing_results)

            if CREATE_MINI_MODS:
                if not dest:
                    mpath = '.\\output\\AutoBuildExamples\\'  # + short_name + '\\'
                else:
                    mpath=dest
                auto.create_mod(mpath, mod.name,CREATE_ZIP)

            processing_results += 'Writing mod took {}.\n'.format(
                elapsed(last))

    return processing_results

def handle_mod(mod_to_handle, vanilla_modinfo,dest=None):
    """Summary

    Args:
        mod (TYPE): Description
        vanilla (TYPE): Description

    Returns:
        TYPE: Description
    """
    processing_results = ''
    with mod_to_handle as mod:
        mod_has_buildings=len(mod_to_handle.buildings()) > 0
        not_ab_submod = 'AutoBuild' not in mod.name and '(AB SubMod)' not in mod.name
        if  mod_has_buildings and not_ab_submod:
            mod.update(vanilla_modinfo)
            try:
                processing_results = generate_mod(mod,dest)
            except:
                processing_results = 'Crash while processing {}:\n'.format(mod.name)
                processing_results += traceback.format_exc()
    return mod_to_handle.name, processing_results

def handle_mod_star(variable_to_unpack):
    """Summary

    Args:
        modpathinfovanilla_buildingslocalizations (TYPE): Description

    Returns:
        TYPE: Description
    """
    return handle_mod(*variable_to_unpack)


def handle_all_mods(moddir=None,dest=None):
    """Loops over mods in file and generates minimods, then reports what it found.
    """
    args = []
    mods=[]
    if moddir:
        mods=ck2.parse_mods_in_dir(moddir)
    else:
        mods=ck2.parse_mods_in_dir()
    if VANILLA.empty():
        #if vanilla hasn't been parsed, I need to front-load at least this to ensure
        #   mods update properly...
        # TODO: figure out a better solution to this.  
        if not all((x.replaces_buildings() for x in mods)):            
            VANILLA.buildings()
        if not all((x.replaces_job_actions() for x in mods)):
            VANILLA.job_actions()
        if not all((x.technology() for x in mods)):
            VANILLA.technology()
        if not all((x.replaces_event_modifiers() for x in mods)):
            VANILLA.event_modifiers()
        if not all((x.replaces_artifacts() for x in mods)):
            VANILLA.artifacts()
        if not all((x.replaces_bloodlines() for x in mods)):
            VANILLA.bloodlines()
    for mod in mods:                    
        if DEBUGGING:
            if 'eheimnisnacht' in mod.name:  # this is for debugging
                mod, result = handle_mod(mod, VANILLA, dest)
                print mod
                print result
        else:
            if USE_MULTIPROCESSING_AT_MOD_LEVEL:
                args.append((mod, VANILLA, dest))
            else:
                mod, result = handle_mod(mod, VANILLA, dest)
                print mod
                print result
    if not DEBUGGING and USE_MULTIPROCESSING_AT_MOD_LEVEL:
        print 'Processing {} mods in parallel'.format(len(args))
        pool = Pool(6)
        res = pool.map(handle_mod_star, args)
        generated_for = []
        for res in res:
            if res[1]:
                print res[1]
                generated_for.append(res[0])
        print "MiniMods generated for", ','.join(generated_for)


def get_profile_stats():
    """Prints the stats to the console.
    """
    profile = pstats.Stats('restats')
    profile.strip_dirs().sort_stats('time').print_stats(10)
    profile.strip_dirs().sort_stats('cumulative').print_stats(10)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--source', dest='source',default='F:\\Games\\Steam\\Crusader Kings II\\',help='CK2 Directory, defaults to F:\\Games\\Steam\\Crusader Kings II\\')
    parser.add_argument('-d','--dest', dest='dest',default=None,help='Destination directory (defaults to ./AutoBuildExamples/')
    parser.add_argument('-m','--moddir', dest='moddir',default=None,help='CK2 Mod directory.  Defaults to Documents/Paradox Interactive/Crusader Kings II/mod/')    
    parser.add_argument('--disable_multiprocessing', dest='multiprocessing',action='store_false',help='Disables multiprocessing at the mod level.')    
    parser.add_argument('--disable_vanilla_gen', dest='gen_vanilla',action='store_false',help='Disables generating mod for vanilla.')
    parser.add_argument('--disable_mod_gen', dest='gen_mods',action='store_false',help='Disables generating minimods.')
    #these are broken currently; zip libraries don't handle unicode terribly well...
    #parser.add_argument('-z','--zip', dest='zip',action='store_true',help='Compresses the generated mods.')
    #parser.add_argument('--convert_to_zip', dest='convert_to_zip',action='store_true',help='Convert found mods to zip files.')
    args = parser.parse_args()        
    USE_MULTIPROCESSING_AT_MOD_LEVEL=args.multiprocessing
    #these are broken currently; zip libraries don't handle unicode terribly well...
    #CREATE_ZIP = args.zip
    #CONVERT_MODS_TO_ZIP = args.convert_to_zip    
    #CK2DIR = 'F:\\Games\\Steam\\Crusader Kings II\\'
    VANILLA = ck2.CK2Info('VANILLA', args.source)
    if args.gen_vanilla:
        if PROFILE:
            cProfile.run('generate_mod(vanilla,args.source,args.dest)', 'restats')
            get_profile_stats()
        else:
            print generate_mod(VANILLA,args.dest)  
    if args.gen_mods:
        handle_all_mods(args.moddir,args.dest)
